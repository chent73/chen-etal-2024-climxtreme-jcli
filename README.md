# chen-etal-2024-climxtreme-JCLI

This repository contains scripts for the paper to be published on the _Journal of Climate_, entitled "**Changes of intense extratropical cyclone deepening mechanisms in a warmer climate in idealized simulations**" by Ting-Chen Chen; Christoph Braun; Aiko Voigt; Joaquim G. Pinto.  

Folders contain:

- Run scripts for the idealized baroclinic life cycle simulations based on the atmospheric component of the Icosahedral Nonhydrostatic model (ICON-NWP; version 2.6.2.2);

- Pre-processing scripts for generating the initial conditions; 

- Post-processing scripts for remapping the model output from native grid to regular lat-lon grid;

- Analysis scripts of PTE (pressure tendency equation) and so on;

- Scrtips to create plots presented in the paper. 

The simulation raw output is archived on the High-Performance Storage System at the German Climate Computing Center (DKRZ). The post-processed data used in the analysis will be published at KITopen of Karlsruhe Institute of Technology (link to be updated).
