# @ Aiko Voigt and Behrooz Keshtgar, KIT
import numpy as np

# numerical derivative of y on unequally-spaced grid x
def get_dydx(y,x):
    dxdy=np.zeros(y.size)+np.nan
    for i in range(1,x.size-1):
        am=-(x[i+1]-x[i]  )/((x[i]  -x[i-1])*(x[i+1]-x[i]+x[i]-x[i-1]))
        ap= (x[i]  -x[i-1])/((x[i+1]-x[i]  )*(x[i+1]-x[i]+x[i]-x[i-1]))
        a0= (x[i+1]-x[i]-(x[i]-x[i-1]))/((x[i+1]-x[i])*(x[i]-x[i-1]))
        dxdy[i]=am*y[i-1] + a0*y[i] + ap*y[i+1]
    return dxdy    
        
def get_verticalintegral(data, plev, plev1, plev2):
    nlev  = plev.size
    dplev = np.zeros(nlev) + np.nan
    dplev[1:nlev-1] = 0.5*(np.abs(plev[2:nlev]-plev[0:nlev-2]))
    dplev[0] = np.abs(plev[1]-plev[0])
    dplev[nlev-1] = np.abs(plev[nlev-1]-plev[nlev-2])
    indlev1 = np.argmin(np.abs(plev-plev1))
    indlev2 = np.argmin(np.abs(plev-plev2))
    out = np.nansum(data[indlev1:indlev2]*dplev[indlev1:indlev2])
    return out

def ddy(f):
    ddyf = (np.roll(f, -1, -2) - np.roll(f, 1, -2)) / (2*dy)
    ddyf[..., 0, :] = 0.
    ddyf[..., -1, :] = 0.
    return ddyf

# physical constants
cp=1004     #specific heat at constant pressure, in J/kg/K
R =287.058  #specific gas constant, in J/kg/K
g =9.80665  #m/s^2
p0=1000e2   #Pa
# Calculating gridlength for deriving horizontal derivities
r_e = 6371200.
dy = 2*np.pi*r_e / 720. # for 0.5x0.5 degree resolution

# calculate zonal-mean available potential energy
def calc_zape(T,lev,lat):
    # input: T   = zonal-mean temperature, dims=(lev,lat)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    
    if np.shape(T)!=(nlev,nlat):
        print('calc_zape: requires zonal-mean data!')
        raise ValueError()    

    # potential temperature
    theta = np.full((nlev,nlat), np.nan, dtype=float)
    for j in range(nlat):
        theta[:,j]=T[:,j]*np.power((p0/lev), R/cp)

    # surface area for domain-mean calculation
    #area = np.cos(lat*np.pi/180)

    # domain-mean data; often this is the global domain 
    # but also can be subdomain, e.g., one hemisphere
    Tgm     = np.full(nlev, np.nan, dtype=float)
    thetagm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        Tgm    [k] = np.average(T[k])#    , weights=area)
        thetagm[k] = np.average(theta[k])#, weights=area)

    # vertical derivative of thetagm wrt pressure
    dthetagmdp = get_dydx(thetagm, lev)

    # gamma is proportional to inverse of dthetagmdp, dims are (nlev)
    gamma = -np.power(p0/lev,R/cp)*(R/(cp*lev))*(1/dthetagmdp)

    # zonal-mean APE for each lev-lat box
    zape = np.full((nlev,nlat), np.nan, dtype=float)
    for j in range(0,nlat):
        zape[:,j]=cp/2*gamma*np.power((T[:,j]-Tgm),2)
        
    # vertically-integrated zonal-mean APE in J/m2
    zape_vint = np.full(nlat, np.nan, dtype=float) 
    for j in range(0,nlat):
        zape_vint[j] = 1/g*get_verticalintegral(zape[:,j], lev, 100e2, 900e2)
        
    # domain-mean of vertically-integrated zonal-mean APE        
    zape_total = np.average(zape_vint)#, weights=area)
    
    return zape_total

# calculate eddy available potential energy
def calc_eape(T,lev,lat,lon):
    # input: T   = temperature, dims=(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat
    #        lon = latitudes in deg lon

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(T)!=(nlev,nlat,nlon):
        print('calc_zape: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean temperature
    Tzm = np.nanmean(T,axis=2)

    # potential temperature
    theta = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for j in range(nlat):
        for k in range(nlon):
            theta[:,j,k]=T[:,j,k]*np.power((p0/lev), R/cp)

    # surface area for domain-mean calculation
    #area = np.full((nlat,nlon), np.nan, dtype=float)
    #for j in range(nlat):
    #    area[j,:] = np.cos(lat[j]*np.pi/180)

    # domain-mean data; often this is the global domain 
    # but also can be subdomain, e.g., one hemisphere
    thetagm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        #thetagm[k] = np.nansum(theta[k]*area)/np.nansum(area)
        thetagm[k] = np.average(theta[k])
    # vertical derivative of thetagm wrt pressure
    dthetagmdp = get_dydx(thetagm, lev)

    # gamma is proportional to inverse of dthetagmdp, dims are (nlev)
    gamma = -np.power(p0/lev,R/cp)*R/(cp*lev)*(1/dthetagmdp)
    
    # eddy APE for each lev-lat-lon box
    eape = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            eape[k,j,:]=cp/2*gamma[k]*np.power(T[k,j,:]-Tzm[k,j],2)
        
    # vertically-integrated eddy APE    
    eape_vint = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):    
            eape_vint[j,i] = 1/g*get_verticalintegral(eape[:,j,i], lev, 100e2, 900e2)
        
    # domain-mean of vertically-integrated eddy APE        
    #eape_total = np.nansum(eape_vint*area)/np.nansum(area)
    eape_total = np.average(eape_vint)
    
    return eape_total


# calculate zonal-mean kinetic energy
def calc_zke(u,v,lev,lat):
    # input: u   = zonal-mean zonal wind, dims=(lev,lat)
    #        v   = zonal-mean meridional wind, dims=(lev,lat)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    
    if (np.shape(u)!=(nlev,nlat)) or (np.shape(v)!=(nlev,nlat)):
        print('calc_zke: requires zonal-mean data!')
        raise ValueError()
        
    # surface area for domain-mean calculation
    #area = np.cos(lat*np.pi/180)

    # zonal-mean KE for each lev-lat box
    zke = 0.5*(np.power(u,2)+np.power(v,2))
    
    # vertically-integrated zonal-mean KE    
    zke_vint = np.full(nlat, np.nan, dtype=float) 
    for j in range(0,nlat):
        zke_vint[j] = 1/g*get_verticalintegral(zke[:,j], lev, 100e2, 900e2)
        
    # domain-mean of vertically-integrated zonal-mean KE        
    zke_total = np.average(zke_vint)#, weights=area)
    
    return zke_total

# calculate eddy kinetic energy
def calc_eke(u,v,lev,lat,lon):
    # input: u   = zonal wind, dims=(lev,lat,lon)
    #        v   = meridional wind, dims=(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat
    #        lon = longitudes in deg lon

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
    
    if (np.shape(u)!=(nlev,nlat,nlon)) or (np.shape(v)!=(nlev,nlat,nlon)):
        print('calc_zke: requires lev-lat-lon data!')
        raise ValueError()
     
    # zonal-mean wind fields
    uzm = np.nanmean(u,axis=2)
    vzm = np.nanmean(v,axis=2)
    
    # surface area for domain-mean calculation
    #area = np.full((nlat,nlon), np.nan, dtype=float)
    #for j in range(nlat):
    #    area[j,:] = np.cos(lat[j]*np.pi/180)
     
    # eddy KE for each lev-lat-lon box
    eke = ( 0.5*(np.power(u-np.expand_dims(uzm,axis=2),2) + 
                 np.power(v-np.expand_dims(vzm,axis=2),2)) )
    
    # vertically-integrated eddy KE    
    eke_vint = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            eke_vint[j,i] = 1/g*get_verticalintegral(eke[:,j,i], lev, 100e2, 900e2)
        
    # domain-mean of vertically-integrated eddy KE
    #eke_total = np.nansum(eke_vint*area)/np.nansum(area)
    eke_total = np.average(eke_vint)
    eke_max = np.max(eke_vint)
    
    return eke_total, eke_max 

# calculate conversion of ZAPE to EAPE
def calc_azae(T,V,W,lev,lat,lon):
    # input: T     = temperature, dims=(lev,lat,lon)
    # input: W(p)  = vertical velocity, dims =(lev,lat,lon)
    # input: V     = meridional velocity, dims=(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(T)!=(nlev,nlat,nlon):
        print('calc_azae: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean temperature, meridional wind and vertical wind
    Tzm = np.nanmean(T,axis=2)
    Vzm = np.nanmean(V,axis=2)
    Wzm = np.nanmean(W,axis=2)
    
    # potential temperature
    theta = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for j in range(nlat):
        for k in range(nlon):
            theta[:,j,k]=T[:,j,k]*np.power((p0/lev), R/cp)
            
    thetazm = np.nanmean(theta,axis=2)   

    # surface area for domain-mean calculation
    #area = np.full((nlat,nlon), np.nan, dtype=float)
    #for j in range(nlat):
    #    area[j,:] = np.cos(lat[j]*np.pi/180)
        
    # surface area 1D for domain-mean calculation
    #area1d = np.cos(lat*np.pi/180)    

    # domain-mean data; often this is the global domain 
    # but also can be subdomain, e.g., one hemisphere
    thetagm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        #thetagm[k] = np.nansum(theta[k]*area)/np.nansum(area)
        thetagm[k] = np.average(theta[k])
    # vertical derivative of thetagm wrt pressure
    dthetagmdp = get_dydx(thetagm, lev)

    # gamma is proportional to inverse of dthetagmdp, dims are (nlev)
    gamma = -np.power(p0/lev,R/cp)*R/(cp*lev)*(1/dthetagmdp)
    
    # meridional temperature gradient
    dtdy = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(nlev):       
            dtdy[k,:,:] = ddy(T[k,:,:])
    
    # eddy azae-part1 for each lev-lat-lon box
    azae1 = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    t_w = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            azae1[k,j,:]=-cp*gamma[k]*(T[k,j,:]-Tzm[k,j])*(V[k,j,:]-Vzm[k,j])*dtdy[k,j,:]
            t_w[k,j,:]=(T[k,j,:]-Tzm[k,j])*(W[k,j,:]-Wzm[k,j])
            
    # domain-mean of t_w
    t_wgm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        #t_wgm[k] = np.nansum(t_w[k]*area)/np.nansum(area)
        t_wgm[k] = np.average(t_w[k])
    
    t_wzm = np.nanmean(t_w,axis=2)    
        
    # vertical derivative of theta-devation wrt pressure
    theta_dev = np.full((nlev,nlat), np.nan, dtype=float)
    for j in range(0,nlat):
        theta_dev[:,j]=thetazm[:,j]-thetagm[:]
    # domain_mean
    theta_devgm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        #theta_devgm[k] = np.average(theta_dev[k]    , weights=area1d)
        theta_devgm[k] = np.average(theta_dev[k])
    
    dtheta_devgmdp = get_dydx(theta_devgm, lev)
    
    # eddy azae-part2 for each lev-lat-lon box
    azae2 = np.full((nlev,nlat), np.nan, dtype=float)
    for j in range(0,nlat):
        azae2[:,j]=-cp*gamma*np.power(lev/p0,R/cp)*(t_wzm[:,j]-t_wgm)*dtheta_devgmdp
                
    # vertically-integrated azae1
    azae_vint1 = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            azae_vint1[j,i] = 1/g*get_verticalintegral(azae1[:,j,i], lev, 100e2, 900e2)
        
    # domain-mean of vertically-integrated eddy AzAe1        
    #azae_d1 = np.nansum(azae_vint1*area)/np.nansum(area)
    azae_d1 = np.average(azae_vint1)
    
    # vertically-integrated eddy azae2
    azae_vint2 = np.full(nlat, np.nan, dtype=float) 
    for j in range(0,nlat):
        azae_vint2[j] = 1/g*get_verticalintegral(azae2[:,j], lev, 100e2, 900e2)
        
    # domain-mean of vertically-integrated zonal-mean AzAe2        
    azae_d2 = np.average(azae_vint2)#, weights=area1d)
    
    return (azae_d1+azae_d2)

# calculate conversion of EAPE to EKE
def calc_aeke(D,W,lev,lat,lon):
    # input: D     = density, dims=(lev,lat,lon)
    # input: W(p)  = vertical velocity, dims =(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(D)!=(nlev,nlat,nlon):
        print('calc_azae: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean specific volume and vertical wind
    Dzm = np.nanmean(D,axis=2)
    Wzm = np.nanmean(W,axis=2)
    
    # surface area for domain-mean calculation
    #area = np.full((nlat,nlon), np.nan, dtype=float)
    #for j in range(nlat):
    #    area[j,:] = np.cos(lat[j]*np.pi/180)
    
    # eddy aeke for each lev-lat-lon box
    aeke = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            aeke[k,j,:]=-((1/D[k,j,:])-(1/Dzm[k,j]))*(W[k,j,:]-Wzm[k,j])
            
    # vertically-integrated eddy aeke
    aeke_vint = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            aeke_vint[j,i] = 1/g*get_verticalintegral(aeke[:,j,i], lev, 100e2, 900e2)
        
    # domain-mean of vertically-integrated eddy APE        
    #aeke_total = np.nansum(aeke_vint*area)/np.nansum(area)
    aeke_total = np.average(aeke_vint)
    
    return aeke_total

# EKE dissipation by friction
def calc_De(u,v,Fu,Fv,lev,lat,lon):
    # input: u     = Zonal wind, dims=(lev,lat,lon)
    # input: v     = meridinal wind, dims =(lev,lat,lon)
    # input: Fu    = zonal wind tendency, dims =(lev,lat,lon)
    # input: Fv    = meridional wind tendency, dims=(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(u)!=(nlev,nlat,nlon):
        print('calc_azae: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean friction and wind velocity
    uzm = np.nanmean(u,axis=2)
    vzm = np.nanmean(v,axis=2)
    fuzm = np.nanmean(Fu,axis=2)
    fvzm = np.nanmean(Fv,axis=2)
    
    # surface area for domain-mean calculation
    area = np.full((nlat,nlon), np.nan, dtype=float)
    for j in range(nlat):
        area[j,:] = np.cos(lat[j]*np.pi/180)
        
    # eddy aeke for each lev-lat-lon box
    de = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            de[k,j,:]=-(((u[k,j,:])-(uzm[k,j]))*((Fu[k,j,:]-fuzm[k,j])) + ((v[k,j,:])-(vzm[k,j]))*((Fv[k,j,:]-fvzm[k,j])))
            
    # vertically-integrated azae1
    de_vint = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            de_vint[j,i] = 1/g*get_verticalintegral(de[:,j,i], lev, 100e2, 900e2)
            
    # domain-mean of vertically-integrated eddy AzAe1        
    de_d = np.nansum(de_vint*area)/np.nansum(area)
    
    return(de_d)

# calculate diabatic generation of EAPE
def calc_dage(T,Q,lev,lat,lon):
    # input: T     = temperature, dims=(lev,lat,lon)
    # input: Q     = condensational heating (K/s), dims =(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(T)!=(nlev,nlat,nlon):
        print('calc_azae: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean temperature, meridional wind and vertical wind
    Tzm = np.nanmean(T,axis=2)
    Qzm = np.nanmean(Q,axis=2)
    
    # potential temperature
    theta = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for j in range(nlat):
        for k in range(nlon):
            theta[:,j,k]=T[:,j,k]*np.power((p0/lev), R/cp)
            
    thetazm = np.nanmean(theta,axis=2)   

    # surface area for domain-mean calculation
    #area = np.full((nlat,nlon), np.nan, dtype=float)
    #for j in range(nlat):
    #    area[j,:] = np.cos(lat[j]*np.pi/180)
        
    # domain-mean data; often this is the global domain 
    # but also can be subdomain, e.g., one hemisphere
    thetagm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        #thetagm[k] = np.nansum(theta[k]*area)/np.nansum(area)
        thetagm[k] = np.average(theta[k])

    # vertical derivative of thetagm wrt pressure
    dthetagmdp = get_dydx(thetagm, lev)

    # gamma is proportional to inverse of dthetagmdp, dims are (nlev)
    gamma = -np.power(p0/lev,R/cp)*R/(cp*lev)*(1/dthetagmdp)
    
    # diabatic generation of EAPE for each lev-lat-lon box
    dage = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            dage[k,j,:]=cp*gamma[k]*(Q[k,j,:]-Qzm[k,j])*(T[k,j,:]-Tzm[k,j])
            
    # vertically-integrated dage
    dage_vint = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            dage_vint[j,i] = 1/g*get_verticalintegral(dage[:,j,i], lev, 100e2, 900e2)
            
    # domain-mean of vertically-integrated dage        
    #dage_total = np.nansum(dage_vint*area)/np.nansum(area)
    dage_total = np.average(dage_vint)
    return dage_total

#----------------------------------------------------------------------------------------
# Another version of conversion rates
#----------------------------------------------------------------------------------------
# calculate conversion of ZAPE to EAPE
def calc_azae2(T,V,W,lev,lat,lon):
    # input: T     = temperature, dims=(lev,lat,lon)
    # input: W(p)  = vertical velocity, dims =(lev,lat,lon)
    # input: V     = meridional velocity, dims=(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(T)!=(nlev,nlat,nlon):
        print('calc_azae: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean temperature, meridional wind and vertical wind
    Tzm = np.nanmean(T,axis=2)
    Vzm = np.nanmean(V,axis=2)
    Wzm = np.nanmean(W,axis=2)
    
    # surface area for domain-mean calculation
    #area = np.full((nlat,nlon), np.nan, dtype=float)
    #for j in range(nlat):
    #    area[j,:] = np.cos(lat[j]*np.pi/180)
        
    # surface area 1D for domain-mean calculation
    #area1d = np.cos(lat*np.pi/180)    
    
    # sigma
    # domain-mean data; often this is the global domain 
    
    tgm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        #tgm[k] = np.nansum(T[k]*area)/np.nansum(area)
        tgm[k] = np.average(T[k])
        
    # vertical derivative of thetagm wrt pressure
    dtgmdp = get_dydx(tgm, lev)    
        
    sigma = g*(tgm/cp-(lev/R)*dtgmdp)
    
    # meridional temperature gradient
    dtdy = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(nlev):       
            dtdy[k,:,:] = ddy(T[k,:,:])
            
    # eddy azae-part1 for each lev-lat-lon box
    azae1 = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            azae1[k,j,:]=(1/sigma[k])*(T[k,j,:]-Tzm[k,j])*(V[k,j,:]-Vzm[k,j])*dtdy[k,j,:]
            
    # vertical derivative of temp-devation
    t_dev = np.full((nlev,nlat), np.nan, dtype=float)
    for j in range(0,nlat):
        t_dev[:,j]=Tzm[:,j]-tgm[:]  
        
    # domain_mean
    t_devgm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        #t_devgm[k] = np.average(t_dev[k]    , weights=area1d)
        t_devgm[k] = np.average(t_dev[k])
    
    t_devgmdp = get_dydx(t_devgm, lev)
    
    # eddy azae-part2 for each lev-lat-lon box
    azae2 = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            azae2[k,j,:]=(1/sigma[k])*(T[k,j,:]-Tzm[k,j])*(W[k,j,:]-Wzm[k,j])*t_devgmdp[k]
                  
    # vertically-integrated azae1
    azae_vint1 = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            azae_vint1[j,i] = get_verticalintegral(azae1[:,j,i], lev, 100e2, 900e2)
            
    # domain-mean of vertically-integrated eddy AzAe1        
    #azae_d1 = np.nansum(azae_vint1*area)/np.nansum(area)
    azae_d1 = np.average(azae_vint1)
    
    # vertically-integrated azae2
    azae_vint2 = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            azae_vint2[j,i] = get_verticalintegral(azae2[:,j,i], lev, 100e2, 900e2)
            
    # domain-mean of vertically-integrated eddy AzAe2        
    #azae_d2 = np.nansum(azae_vint2*area)/np.nansum(area)
    azae_d2 = np.average(azae_vint2)
    
    return(-azae_d1-azae_d2)


# calculate conversion of EAPE to EKE
def calc_aeke2(T,W,lev,lat,lon):
    # input: T     = temp, dims=(lev,lat,lon)
    # input: W(p)  = vertical velocity, dims =(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(T)!=(nlev,nlat,nlon):
        print('calc_azae: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean specific volume and vertical wind
    Tzm = np.nanmean(T,axis=2)
    Wzm = np.nanmean(W,axis=2)
    
    # surface area for domain-mean calculation
    #area = np.full((nlat,nlon), np.nan, dtype=float)
    #for j in range(nlat):
    #    area[j,:] = np.cos(lat[j]*np.pi/180)
        
    # eddy aeke for each lev-lat-lon box
    aeke = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            aeke[k,j,:]=-(R/lev[k])*(T[k,j,:]-Tzm[k,j])*(W[k,j,:]-Wzm[k,j])
            
    # vertically-integrated azae1
    aeke_vint = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            aeke_vint[j,i] = 1/g*get_verticalintegral(aeke[:,j,i], lev, 100e2, 900e2)
            
    # domain-mean of vertically-integrated eddy AzAe1        
    #aeke_d = np.nansum(aeke_vint*area)/np.nansum(area)
    aeke_d = np.average(aeke_vint)
    
    return(aeke_d)

# calculate diabatic generation of EAPE
def calc_dage2(T,Q,lev,lat,lon):
    # input: T     = temperature, dims=(lev,lat,lon)
    # input: Q     = condensational heating (K/s), dims =(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(T)!=(nlev,nlat,nlon):
        print('calc_azae: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean temperature, meridional wind and vertical wind
    Tzm = np.nanmean(T,axis=2)
    Qzm = np.nanmean(Q,axis=2)
    
    # surface area for domain-mean calculation
    #area = np.full((nlat,nlon), np.nan, dtype=float)
    #for j in range(nlat):
    #    area[j,:] = np.cos(lat[j]*np.pi/180)
    
    # sigma
    # domain-mean data; often this is the global domain 
    
    tgm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        #tgm[k] = np.nansum(T[k]*area)/np.nansum(area)
        tgm[k] = np.average(T[k])
        
    # vertical derivative of thetagm wrt pressure
    dtgmdp = get_dydx(tgm, lev)    
        
    sigma = g*(tgm/cp-(lev/R)*dtgmdp)
    
    # dage for each lev-lat-lon box
    dage = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            dage[k,j,:]=(1/sigma[k])*(T[k,j,:]-Tzm[k,j])*(Q[k,j,:]-Qzm[k,j])
    
    # vertically-integrated azae1
    dage_vint = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            dage_vint[j,i] = get_verticalintegral(dage[:,j,i], lev, 100e2, 900e2)
            
    # domain-mean of vertically-integrated dage        
    #dage_d = np.nansum(dage_vint*area)/np.nansum(area)
    dage_d = np.average(dage_vint)
    
    return(dage_d)        
#----------------------------------------------------------------------------------
# EKE on model levels
#----------------------------------------------------------------------------------
def calc_eke_ml(D,u,v,lev,lat,lon):
    
    # input: D     = density, dims=(lev,lat,lon)
    # input: u     = zonal wind, dims =(lev,lat,lon)
    # input: v     = merdional wind, dims =(lev,lat,lon)
    #        lev = model levels
    #        lat = latitudes in deg lat
    
    nlat = lat.size
    nlon = lon.size
    nlev = lev.size
    
    # surface area for domain-mean calculation
    area = np.full((nlat,nlon), np.nan, dtype=float)
    for j in range(nlat):
        area[j,:] = np.cos(lat[j]*np.pi/180)
    
    # zonal-mean wind fields
    uzm = np.nanmean(u,axis=2)
    vzm = np.nanmean(v,axis=2)
    
    # domain-mean density
    
    d0 = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        d0[k]     = np.nansum(D[k]*area)/np.nansum(area)
        
    # eddy KE for each lev-lat-lon box
    eke = ( np.expand_dims(d0,axis=(1, 2))*0.5*(np.power(u-np.expand_dims(uzm,axis=2),2) + 
                 np.power(v-np.expand_dims(vzm,axis=2),2)) ) 
    
    eke_vint = np.nanmean(eke,axis=0)
    
    eke_total = np.nansum(eke_vint*area)/np.nansum(area)
    
    return eke_total

#-----------------------
# NEEDS RECHECKING !!
#-----------------------
# calculate conversion of ZAPE to EAPE
def calc_azae3(T,V,W,lev,lat,lon):
    # input: T     = temperature, dims=(lev,lat,lon)
    # input: W(p)  = vertical velocity, dims =(lev,lat,lon)
    # input: V     = meridional velocity, dims=(lev,lat,lon)
    #        lev = pressure levels in Pa
    #        lat = latitudes in deg lat

    nlev = lev.size
    nlat = lat.size
    nlon = lon.size
     
    if np.shape(T)!=(nlev,nlat,nlon):
        print('calc_azae: requires lev-lat-lon data!')
        raise ValueError()    

    # zonal-mean temperature, meridional wind and vertical wind
    Tzm = np.nanmean(T,axis=2)
    Vzm = np.nanmean(V,axis=2)
    Wzm = np.nanmean(W,axis=2)
    
    # potential temperature
    theta = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for j in range(nlat):
        for k in range(nlon):
            theta[:,j,k]=T[:,j,k]*np.power((p0/lev), R/cp)

    # surface area for domain-mean calculation
    area = np.full((nlat,nlon), np.nan, dtype=float)
    for j in range(nlat):
        area[j,:] = np.cos(lat[j]*np.pi/180)

    # domain-mean data; often this is the global domain 
    # but also can be subdomain, e.g., one hemisphere
    thetagm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        thetagm[k] = np.nansum(theta[k]*area)/np.nansum(area)

    # vertical derivative of thetagm wrt pressure
    dthetagmdp = get_dydx(thetagm, lev)

    # gamma is proportional to inverse of dthetagmdp, dims are (nlev)
    gamma = -np.power(p0/lev,R/cp)*R/(cp*lev)*(1/dthetagmdp)
    
    # meridional temperature gradient
    dtdy = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(nlev):       
            dtdy[k,:,:] = ddy(T[k,:,:])
    
    # domain-mean meridional temperature gradient        
    dtdygm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        dtdygm[k] = np.nansum(dtdy[k]*area)/np.nansum(area)
    
    # eddy azae-part1 for each lev-lat-lon box
    azae1 = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    t_w = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            azae1[k,j,:]=-cp*gamma[k]*(T[k,j,:]-Tzm[k,j])*(V[k,j,:]-Vzm[k,j])*dtdygm[k]
            t_w[k,j,:]=(T[k,j,:]-Tzm[k,j])*(W[k,j,:]-Wzm[k,j])
            
    # domain-mean of t_w
    t_wgm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        t_wgm[k] = np.nansum(t_w[k]*area)/np.nansum(area)
        
    # vertical derivative of theta-devation wrt pressure
    theta_dev = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            for i in range(0,nlon):
                theta_dev[k,j,i]=theta[k,j,i]-thetagm[k]
    # domain_mean
    theta_devgm = np.full(nlev, np.nan, dtype=float)
    for k in range(nlev):
        theta_devgm[k] = np.nansum(theta_dev[k]*area)/np.nansum(area)
    
    dtheta_devgmdp = get_dydx(theta_devgm, lev)
    
    # eddy azae-part2 for each lev-lat-lon box
    azae2 = np.full((nlev,nlat,nlon), np.nan, dtype=float)
    for k in range(0,nlev):
        for j in range(0,nlat):
            for i in range(0,nlon):
                azae2[k,j,i]=-cp*gamma[k]*np.power(lev[k]/p0,R/cp)*(t_w[k,j,i]-t_wgm[k])*dtheta_devgmdp[k]
                
   # vertically-integrated eddy azae
    azae = azae1 + azae2
    azae_vint = np.full((nlat,nlon), np.nan, dtype=float) 
    for j in range(0,nlat):
        for i in range(0,nlon):
            azae_vint[j,i] = 1/g*get_verticalintegral(azae[:,j,i], lev, 100e2, 900e2)
        
    # domain-mean of vertically-integrated eddy AzAe        
    azae_total = np.nansum(azae_vint*area)/np.nansum(area)
    
    return azae_total
