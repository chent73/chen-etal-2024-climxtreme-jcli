#!/bin/bash
#=============================================================================
#SBATCH --account=bb1152
#SBATCH --job-name=copy_atm_2km
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --threads-per-core=2
# the following is needed to work around a bug that otherwise leads to
# a too low number of ranks when using compute,compute2 as queue
#SBATCH --mem=0
#SBATCH --exclusive
#SBATCH --output=/work/bb1152/Module_A/A6_CyclEx/b380782_Christoph/pp_logs/LOG.copy_atm_2km.%j.o
#SBATCH --error=/work/bb1152/Module_A/A6_CyclEx/b380782_Christoph/pp_logs/LOG.copy_atm_2km.%j.o
#SBATCH --time=02:00:00
#=========================================================================================
# this script is for copying data sets from scratch to work

# define directories
exp=channel_2km_0006
inpath=/scratch/b/b380906 # directory with data that will be copied
projpath=/work/bb1152/Module_A/A6_CyclEx # directory of project on work
cppath=sim_data/production # directory that data will be copied to

simpath=$inpath/$exp
echo $simpath

outpath=$projpath/$cppath/$exp
echo $outpath

# create folder in which the data will be stored
mkdir -p $outpath
cd $outpath

cp $simpath/icon-extra_ml.nc $outpath

#------------------------------------------------------------------------------------------
# loop over the time steps
for day in 01 02 03 04 05 06 07 08 09 ; do

for hour in 00 06 12 18 ; do

cp $simpath/*atm2d*${day}T${hour}0000Z.nc $outpath
cp $simpath/*atm3d*${day}T${hour}0000Z.nc $outpath
cp $simpath/*ddt3d*${day}T${hour}0000Z.nc $outpath

done
done

day=10
hour=00

cp $simpath/*atm2d*${day}T${hour}0000Z.nc $outpath
cp $simpath/*atm3d*${day}T${hour}0000Z.nc $outpath
cp $simpath/*ddt3d*${day}T${hour}0000Z.nc $outpath
