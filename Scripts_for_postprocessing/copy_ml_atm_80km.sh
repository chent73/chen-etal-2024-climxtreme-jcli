#!/bin/bash
#=============================================================================
#SBATCH --account=bb1152
#SBATCH --job-name=copy_atm_80km
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --threads-per-core=2
# the following is needed to work around a bug that otherwise leads to
# a too low number of ranks when using compute,compute2 as queue
#SBATCH --mem=0
#SBATCH --exclusive
#SBATCH --output=/work/bb1152/Module_A/A6_CyclEx/b380782_Christoph/pp_logs/LOG.copy_atm_80km.%j.o
#SBATCH --error=/work/bb1152/Module_A/A6_CyclEx/b380782_Christoph/pp_logs/LOG.copy_atm_80km.%j.o
#SBATCH --time=01:00:00
#=========================================================================================
# this script is for copying data sets from scratch to work

# define directories
exp=channel_80km_0001
inpath=/scratch/b/b380906 # directory with data that will be copied
projpath=/work/bb1152/Module_A/A6_CyclEx # directory of project on work
cppath=sim_data/production # directory that data will be copied to

simpath=$inpath/$exp
echo $simpath

outpath=$projpath/$cppath/$exp
echo $outpath

# create folder in which the data will be stored
mkdir -p $outpath
cd $outpath

# cp $simpath/icon-extra_ml.nc $outpath
cp $simpath/*2d* $outpath
# cp $simpath/*3d*.nc $outpath
