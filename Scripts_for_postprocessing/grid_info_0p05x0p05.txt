#
# gridID 1
#
gridtype  = lonlat
gridsize  = 1637100
xsize     = 1020
ysize     = 1605
xname     = lon
xlongname = "longitude"
xunits    = "degrees_east"
yname     = lat
ylongname = "latitude"
yunits    = "degrees_north"
xfirst    = 12.5
xinc      = 0.05
yfirst    = 4.5
yinc      = 0.05
