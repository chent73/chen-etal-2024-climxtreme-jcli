#!/bin/bash
#=============================================================================
#SBATCH --account=bb1152
#SBATCH --job-name=interpolate_mlpl_qv_u_v_850hPa_2km
#SBATCH --partition=compute
#SBATCH --nodes=10
#SBATCH --threads-per-core=2
# the following is needed to work around a bug that otherwise leads to
# a too low number of ranks when using compute,compute2 as queue
#SBATCH --mem=0
#SBATCH --exclusive
#SBATCH --output=/home/b/b380782/pp_logs_cyclex/LOG.interpolate_mlpl_qv_u_v_850hPa_2km.o%j
#SBATCH --error=/home/b/b380782/pp_logs_cyclex/LOG.interpolate_mlpl_qv_u_v_850hPa_2km.e%j
#SBATCH --time=02:00:00
#=========================================================================================
# this script is for interpolating the variables qv, u, and v from model levels to 850 hPa (staying on the native triangular grid)

# folder with data that will be remapped
exp=channel_2km_0006
datapath=/work/bb1152/Module_A/A6_CyclEx/sim_data/production
projpath=/work/bb1152/Module_A/A6_CyclEx

simpath=$datapath/$exp
echo $simpath

# folder in which the remapped data will be stored
mkdir -p $simpath/interpol_850hPa_qv_u_v_native
cd $simpath/interpol_850hPa_qv_u_v_native

# define pressure levels that data will be remapped to
plevout="85000"

#------------------------------------------------------------------------------------------
# loop over the time steps
for day in 01 02 03 04 05 06 07 08 09 ; do

for hour in 00 06 12 18 ; do

# 1) select variables from the 3d dataset

cdo -P 38 selname,pres,u,v,qv ../icon-atm3d_ML_202101${day}T${hour}0000Z.nc icon-3d_ML_202101${day}T${hour}0000Z.nc

# 2) Interpolate from height to pressure levels (var 'pres' must be present in the dataset)

# get sfc pressure from 2d files
cdo -P 38 selname,pres_sfc ../icon-atm2d_ML_202101${day}T${hour}0000Z.nc pres_sfc_202101${day}T${hour}0000Z.nc

# merge sfc pressure into remapped 3d files
cdo -P 38 merge icon-3d_ML_202101${day}T${hour}0000Z.nc pres_sfc_202101${day}T${hour}0000Z.nc icon-3d_ML_202101${day}T${hour}0000Z.psfc.nc

# interpolate to pressure levels
cdo -P 38 ap2pl,$plevout icon-3d_ML_202101${day}T${hour}0000Z.psfc.nc icon-qv_u_v_850hPa_202101${day}T${hour}0000Z.nc

# cleanup
rm icon-3d_ML_202101${day}T${hour}0000Z.nc pres_sfc_202101${day}T${hour}0000Z.nc icon-3d_ML_202101${day}T${hour}0000Z.psfc.nc

done
done

#-----------------------------------------------------------------------------------------
# last time step

day=10
hour=00

# 1) select variables from the 3d dataset

cdo -P 38 selname,pres,u,v,qv ../icon-atm3d_ML_202101${day}T${hour}0000Z.nc icon-3d_ML_202101${day}T${hour}0000Z.nc

# 2) Interpolate from height to pressure levels (var 'pres' must be present in the dataset)

# get sfc pressure from 2d files
cdo -P 38 selname,pres_sfc ../icon-atm2d_ML_202101${day}T${hour}0000Z.nc pres_sfc_202101${day}T${hour}0000Z.nc

# merge sfc pressure into remapped 3d files
cdo -P 38 merge icon-3d_ML_202101${day}T${hour}0000Z.nc pres_sfc_202101${day}T${hour}0000Z.nc icon-3d_ML_202101${day}T${hour}0000Z.psfc.nc

# interpolate to pressure levels
cdo -P 38 ap2pl,$plevout icon-3d_ML_202101${day}T${hour}0000Z.psfc.nc icon-qv_u_v_850hPa_202101${day}T${hour}0000Z.nc

# cleanup
rm icon-3d_ML_202101${day}T${hour}0000Z.nc pres_sfc_202101${day}T${hour}0000Z.nc icon-3d_ML_202101${day}T${hour}0000Z.psfc.nc

#
