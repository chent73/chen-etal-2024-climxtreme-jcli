#!/bin/bash
#=============================================================================
#SBATCH --account=bb1152
#SBATCH --job-name=remap_atm2d_2km
#SBATCH --partition=compute
#SBATCH --nodes=10
#SBATCH --threads-per-core=2
# the following is needed to work around a bug that otherwise leads to
# a too low number of ranks when using compute,compute2 as queue
#SBATCH --mem=0
#SBATCH --exclusive
#SBATCH --output=/home/b/b380782/pp_logs_cyclex/LOG.remap_atm2d_2km.o%j
#SBATCH --error=/home/b/b380782/pp_logs_cyclex/LOG.remap_atm2d_2km.e%j
#SBATCH --time=1:00:00
#=========================================================================================
# this script is for remapping data sets from native unstructured grid 
# to regular lat/lon grid
# first near neighbor interpolation with the same resolution then conservative remapping


#### INFO: If you cloned the code repository to your home directory (as suggested in the wiki)
####       you need to change the username and experiment in the following section. The username
####       also needs to be changed in the job header (path for log and error files).

#### Set username and experiment
###
## Set username here and in job header
#
# name of user submitting this script
username=b380782

# experiment name
exp=channel_2km_0005

####
####

#### load required module
module load cdo

#### Set paths

### raw data
# folder with data that will be remapped
datapath=/work/bb1152/Module_A/A6_CyclEx/sim_data/production

simpath=$datapath/$exp
echo $simpath

### required scripts
# path to clone of code repository
repopath=/home/b/$username/icon-climxtreme
# path to scripts WITHIN code repository
scriptpath=CC_postprocessing_scripts

### output directory
# indicated scratch with predefined directory
outpath=/scratch/b/$username/check_remapping_on_PTE/$exp/
echo $outpath

# directory, in which the remapped data will be stored
mkdir -p $outpath/remapped_atm2d_latlon
cd $outpath/remapped_atm2d_latlon

#### Remapping

# 1) Creating weights from a dataset which contains grid information!
# icon-extra_ml.nc is the dataset which contains the clon,clat or from the grid file itself

# Grid description: grid_info_2km.txt

cdo -P 32 gennn,$repopath/$scriptpath/grid_info_2km.txt $simpath/icon-extra_ml.nc remapweights_2km.nc

#------------------------------------------------------------------------------------------
# loop over the time steps
for day in 01 02 03 04 05 06 07 08 09 ; do

for hour in 00 06 12 18 ; do

# 2) remap to regular lat,lon grid (near neighbor)

# remap all variables
cdo -P 38 remap,$repopath/$scriptpath/grid_info_2km.txt,remapweights_2km.nc $simpath/icon-atm2d_ML_202101${day}T${hour}0000Z.nc icon-atm2d_ML_reg_202101${day}T${hour}0000Z.nc

# 3) conservative remapping to 0.25x0.25 degree

cdo -P 32 remapcon,$repopath/$scriptpath/grid_info_2km_to_1x1.txt icon-atm2d_ML_reg_202101${day}T${hour}0000Z.nc icon-atm2d_ML_reg_con_202101${day}T${hour}0000Z.nc

rm icon-atm2d_ML_reg_202101${day}T${hour}0000Z.nc

done
done

#-----------------------------------------------------------------------------------------
# last time step

day=10
hour=00

# 2) remap to regular lat,lon grid (near neighbor)

# remap all variables
cdo -P 38 remap,$repopath/$scriptpath/grid_info_2km.txt,remapweights_2km.nc $simpath/icon-atm2d_ML_202101${day}T${hour}0000Z.nc icon-atm2d_ML_reg_202101${day}T${hour}0000Z.nc

# 3) conservative remapping to 1x1 degree

cdo -P 32 remapcon,$repopath/$scriptpath/grid_info_2km_to_1x1.txt icon-atm2d_ML_reg_202101${day}T${hour}0000Z.nc icon-atm2d_ML_reg_con_202101${day}T${hour}0000Z.nc

rm icon-atm2d_ML_reg_202101${day}T${hour}0000Z.nc

#-----------------------------------------------------------------------------------------
# delete grid file and remap weights

rm remapweights_2km.nc

#
