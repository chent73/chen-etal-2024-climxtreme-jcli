#!/bin/bash
#=============================================================================
#SBATCH --account=bb1152
#SBATCH --job-name=remap_mlpl_atm3d_80km
#SBATCH --partition=compute
#SBATCH --nodes=10
#SBATCH --threads-per-core=2
# the following is needed to work around a bug that otherwise leads to
# a too low number of ranks when using compute,compute2 as queue
#SBATCH --mem=0
#SBATCH --exclusive
#SBATCH --output=/work/bb1152/Module_A/A6_CyclEx/b380782_Christoph/pp_logs/LOG.remap_mlpl_atm3d_80km.%j.o
#SBATCH --error=/work/bb1152/Module_A/A6_CyclEx/b380782_Christoph/pp_logs/LOG.remap_mlpl_atm3d_80km.%j.o
#SBATCH --time=00:30:00
#=========================================================================================
# this script is for remapping data sets from native unstructured grid 
# to regular lat/lon grid
# first near neighbor interpolation with the same resolution then conservative remapping

# folder with data that will be remapped
exp=channel_80km_0006
datapath=/work/bb1152/Module_A/A6_CyclEx/sim_data/production
projpath=/work/bb1152/Module_A/A6_CyclEx

simpath=$datapath/$exp
echo $simpath

# folder in which the remapped data will be stored
mkdir -p $simpath/remapped_atm3d_latlon
cd $simpath/remapped_atm3d_latlon

# define pressure levels that data will be remapped to
plevout="1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,11000,12000,13000,14000,15000,16000,17000,18000,19000,20000,21000,22000,23000,24000,25000,26000,27000,28000,29000,30000,31000,32000,33000,34000,35000,36000,37000,38000,39000,40000,41000,42000,43000,44000,45000,46000,47000,48000,49000,50000,51000,52000,53000,54000,55000,56000,57000,58000,59000,60000,61000,62000,63000,64000,65000,66000,67000,68000,69000,70000,71000,72000,73000,74000,75000,76000,77000,78000,79000,80000,81000,82000,83000,84000,85000,86000,87000,88000,89000,90000,91000,92000,93000,94000,95000,96000,97000,98000,99000,100000"

# 1) Creating weights from a dataset which contains grid information!
# icon-extra_ml.nc is the dataset which contains the clon,clat or from the grid file itself

# Grid description: grid_info_80km.txt

cdo -P 32 gennn,$projpath/b380782_Christoph/icon-climxtreme/postprocessing_scripts/grid_info_80km.txt $simpath/icon-extra_ml.nc remapweights_80km.nc

#------------------------------------------------------------------------------------------
# loop over the time steps
for day in 01 02 03 04 05 06 07 08 09 ; do

for hour in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 ; do

# 2) select variables from the 3d dataset

cdo -P 38 selname,u,v,pres,w,omega,temp,vor,div,z_ifc,geopot,qv ../icon-atm3d_ML_202101${day}T${hour}0000Z.nc icon-3d_ML_202101${day}T${hour}0000Z.nc

# 3) remap to regular lat,lon grid (near neighbor)

cdo -P 38 remap,$projpath/b380782_Christoph/icon-climxtreme/postprocessing_scripts/grid_info_80km.txt,remapweights_80km.nc icon-3d_ML_202101${day}T${hour}0000Z.nc icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc

rm icon-3d_ML_202101${day}T${hour}0000Z.nc

# 4) conservative remapping to 1x1 degree

cdo -P 32 remapcon,$projpath/b380782_Christoph/icon-climxtreme/postprocessing_scripts/grid_info_1x1.txt icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.nc

rm icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc

# 5) Interpolate from height to pressure levels (var 'pres' must be present in the dataset)

# get sfc pressure from remapped 2d files
cdo -P 38 selname,pres_sfc ../remapped_atm2d_latlon/icon-atm2d_ML_reg_con_202101${day}T${hour}0000Z.nc pres_sfc_reg_con_202101${day}T${hour}0000Z.nc

# merge sfc pressure into remapped 3d files
cdo -P 38 merge icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.nc pres_sfc_reg_con_202101${day}T${hour}0000Z.nc icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc

# interpolate to pressure levels
cdo -P 38 ap2pl,$plevout icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc icon-atm3d_PL_reg_con_202101${day}T${hour}0000Z.nc

# cleanup
rm pres_sfc_reg_con_202101${day}T${hour}0000Z.nc icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc

done
done

#-----------------------------------------------------------------------------------------
# last time step

day=10
hour=00

# 2) select variables from the 3d dataset

cdo -P 38 selname,u,v,pres,w,omega,temp,vor,div,z_ifc,geopot,qv ../icon-atm3d_ML_202101${day}T${hour}0000Z.nc icon-3d_ML_202101${day}T${hour}0000Z.nc

# 3) remap to regular lat,lon grid (near neighbor)

cdo -P 38 remap,$projpath/b380782_Christoph/icon-climxtreme/postprocessing_scripts/grid_info_80km.txt,remapweights_80km.nc icon-3d_ML_202101${day}T${hour}0000Z.nc icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc

rm icon-3d_ML_202101${day}T${hour}0000Z.nc

# 4) conservative remapping to 1x1 degree

cdo -P 32 remapcon,$projpath/b380782_Christoph/icon-climxtreme/postprocessing_scripts/grid_info_1x1.txt icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.nc

rm icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc

# 5) Interpolate from height to pressure levels (var 'pres' must be present in the dataset)

# get sfc pressure from remapped 2d files
cdo -P 38 selname,pres_sfc ../remapped_atm2d_latlon/icon-atm2d_ML_reg_con_202101${day}T${hour}0000Z.nc pres_sfc_reg_con_202101${day}T${hour}0000Z.nc

# merge sfc pressure into remapped 3d files
cdo -P 38 merge icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.nc pres_sfc_reg_con_202101${day}T${hour}0000Z.nc icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc

# interpolate to pressure levels
cdo -P 38 ap2pl,$plevout icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc icon-atm3d_PL_reg_con_202101${day}T${hour}0000Z.nc

# cleanup
rm pres_sfc_reg_con_202101${day}T${hour}0000Z.nc icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc

#-----------------------------------------------------------------------------------------
# delete grid file and remap weights
rm remapweights_80km.nc

#
