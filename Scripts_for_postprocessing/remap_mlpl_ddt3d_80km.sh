#!/bin/bash
#=============================================================================
#SBATCH --account=bb1152
#SBATCH --job-name=remap_mlpl_ddt3d_80km
#SBATCH --partition=compute
#SBATCH --nodes=10
#SBATCH --threads-per-core=2
# the following is needed to work around a bug that otherwise leads to
# a too low number of ranks when using compute,compute2 as queue
#SBATCH --mem=0
#SBATCH --exclusive
#SBATCH --output=/home/b/b380782/pp_logs_cyclex/LOG.remap_mlpl_ddt3d_80km.%j.o
#SBATCH --error=/home/b/b380782/pp_logs_cyclex/LOG.remap_mlpl_ddt3d_80km.%j.e
#SBATCH --time=04:00:00
#=========================================================================================
# this script is for remapping data sets from native unstructured grid 
# to regular lat/lon grid
# first near neighbor interpolation with the same resolution then conservative remapping


#### INFO: If you cloned the code repository to your home directory (as suggested in the wiki)
####       you need to change the username and experiment in the following section. The username
####       also needs to be changed in the job header (path for log and error files).

#### Set username and experiment
###
## Set username here and in job header
#
# name of user submitting this script
username=b380782

# experiment name
exp=channel_80km_0001

####
####

#### load required module
module load cdo

#### Set paths

### raw data
# folder with data that will be remapped
datapath=/work/bb1152/Module_A/A6_CyclEx/sim_data/production

simpath=$datapath/$exp/raw_data
echo $simpath

### required scripts
# path to clone of code repository
repopath=/home/b/$username/icon-climxtreme
# path to scripts WITHIN code repository
scriptpath=CC_postprocessing_scripts

### output directory
# indicated scratch with predefined directory
outpath=/work/bb1152/Module_A/A6_CyclEx/sim_data/production/$exp
echo $outpath

# directory, in which the remapped data will be stored
mkdir -p $outpath/remapped_ddt3d_latlon
cd $outpath/remapped_ddt3d_latlon

#### Remapping

# define pressure levels that data will be remapped to
plevout="1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,11000,12000,13000,14000,15000,16000,17000,18000,19000,20000,21000,22000,23000,24000,25000,26000,27000,28000,29000,30000,31000,32000,33000,34000,35000,36000,37000,38000,39000,40000,41000,42000,43000,44000,45000,46000,47000,48000,49000,50000,51000,52000,53000,54000,55000,56000,57000,58000,59000,60000,61000,62000,63000,64000,65000,66000,67000,68000,69000,70000,71000,72000,73000,74000,75000,76000,77000,78000,79000,80000,81000,82000,83000,84000,85000,86000,87000,88000,89000,90000,91000,92000,93000,94000,95000,96000,97000,98000,99000,100000"

# 1) Creating weights from a dataset which contains grid information!
# icon-extra_ml.nc is the dataset which contains the clon,clat or from the grid file itself

# Grid description: grid_info_80km.txt

cdo -P 32 gennn,$repopath/$scriptpath/grid_info_80km.txt $simpath/icon-extra_ml.nc remapweights_80km.nc

#------------------------------------------------------------------------------------------
# loop over the time steps
for day in 01 02 03 04 05 06 07 08 09 ; do

for hour in 00 06 12 18 ; do

# 2) remap to regular lat,lon grid (near neighbor)

cdo -P 38 remap,$repopath/$scriptpath/grid_info_80km.txt,remapweights_80km.nc $simpath/icon-ddt3d_ML_202101${day}T${hour}0000Z.nc icon-ddt3d_ML_reg_202101${day}T${hour}0000Z.nc

# 3) conservative remapping to 1x1 degree

cdo -P 32 remapcon,$repopath/$scriptpath/grid_info_1x1.txt icon-ddt3d_ML_reg_202101${day}T${hour}0000Z.nc icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.nc

rm icon-ddt3d_ML_reg_202101${day}T${hour}0000Z.nc

# 4) Interpolate from height to pressure levels (var 'pres' must be present in the dataset)

# get sfc pressure from remapped 2d files
cdo -P 38 selname,pres_sfc ../remapped_atm2d_latlon/icon-atm2d_ML_reg_con_202101${day}T${hour}0000Z.nc pres_sfc_reg_con_202101${day}T${hour}0000Z.nc

# merge sfc pressure into remapped 3d files
cdo -P 38 merge icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.nc pres_sfc_reg_con_202101${day}T${hour}0000Z.nc icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc

# interpolate to pressure levels
cdo -P 38 ap2pl,$plevout icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc icon-ddt3d_PL_reg_con_202101${day}T${hour}0000Z.nc

# cleanup
rm pres_sfc_reg_con_202101${day}T${hour}0000Z.nc icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc

done
done

#-----------------------------------------------------------------------------------------
# last time step

day=10
hour=00

# 2) remap to regular lat,lon grid (near neighbor)

cdo -P 38 remap,$repopath/$scriptpath/grid_info_80km.txt,remapweights_80km.nc $simpath/icon-ddt3d_ML_202101${day}T${hour}0000Z.nc icon-ddt3d_ML_reg_202101${day}T${hour}0000Z.nc

# 3) conservative remapping to 1x1 degree

cdo -P 32 remapcon,$repopath/$scriptpath/grid_info_1x1.txt icon-ddt3d_ML_reg_202101${day}T${hour}0000Z.nc icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.nc

rm icon-ddt3d_ML_reg_202101${day}T${hour}0000Z.nc

# 4) Interpolate from height to pressure levels (var 'pres' must be present in the dataset)

# get sfc pressure from remapped 2d files
cdo -P 38 selname,pres_sfc ../remapped_atm2d_latlon/icon-atm2d_ML_reg_con_202101${day}T${hour}0000Z.nc pres_sfc_reg_con_202101${day}T${hour}0000Z.nc

# merge sfc pressure into remapped 3d files
cdo -P 38 merge icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.nc pres_sfc_reg_con_202101${day}T${hour}0000Z.nc icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc

# interpolate to pressure levels
cdo -P 38 ap2pl,$plevout icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc icon-ddt3d_PL_reg_con_202101${day}T${hour}0000Z.nc

# cleanup
rm pres_sfc_reg_con_202101${day}T${hour}0000Z.nc icon-ddt3d_ML_reg_con_202101${day}T${hour}0000Z.psfc.nc

#-----------------------------------------------------------------------------------------
# delete grid file and remap weights
rm remapweights_80km.nc

#
