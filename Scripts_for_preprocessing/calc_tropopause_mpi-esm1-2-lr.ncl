begin
;************************************************
; open file and read in data
;************************************************
; prepare the input file with get_Tanom_from_MPI-ESM_plev.sh

  ;DataFileName = "clim3D_Amon_MPI-ESM1-2-LR_historical_r1i1p1f1_1980-81to2009-10.DJFmean.90w40e20n80n.zonmean.nc"
  ;DataFileName = "clim3D_Amon_MPI-ESM1-2-LR_ssp585_r1i1p1f1_2020-21to2049-50.DJFmean.90w40e20n80n.zonmean.nc"
  DataFileName = "clim3D_Amon_MPI-ESM1-2-LR_ssp585_r1i1p1f1_2070-71to2099-00.DJFmean.90w40e20n80n.zonmean.nc"
  in  = addfile(DataFileName,"r")
  t   = in->ta(0,:,:,0)
  lev = in->plev(:)
  lat = in->lat(:)

  printVarSummary(t)

;************************************************
; calculate tropopause
;************************************************
  ; Invert variables, pressure must be monotonically *increasing*
  ; which is not the case in MPI-ESM1-2-LR
  t_trans = transpose(t(::-1,:))
  lev_trans = lev(::-1)
  printVarSummary(t_trans)

  ; The order of the dimensions is different from what trop_wmo is
  ; expecting -> change the dimension to (lat,lev) instead of 
  ; (lev,lat)
  ptrop = trop_wmo(lev_trans, t_trans(lat|:,plev|:), 1, False)

  ;print(ptrop)

  ptrop!0      ="lat"
;  ptrop!1      ="lon"
  ptrop&lat    = t&lat
;  ptrop&lon    = t&lon
  printVarSummary(ptrop)
;************************************************
; save to netcdf file
;************************************************
  ; historical
  ;system("/bin/rm -f clim3D_Amon_MPI-ESM1-2-LR_historical_tropopause_zonmean.nc") ; remove any pre-existing file
  ;ncdf = addfile("clim3D_Amon_MPI-ESM1-2-LR_historical_tropopause_zonmean.nc" ,"c")  ; open output netCDF file
  ; near future
  ;system("/bin/rm -f clim3D_Amon_MPI-ESM1-2-LR_ssp585_nearfuture_tropopause_zonmean.nc") ; remove any pre-existing file
  ;ncdf = addfile("clim3D_Amon_MPI-ESM1-2-LR_ssp585_nearfuture_tropopause_zonmean.nc" ,"c")  ; open output netCDF file
  ; far future
  system("/bin/rm -f clim3D_Amon_MPI-ESM1-2-LR_ssp585_farfuture_tropopause_zonmean.nc") ; remove any pre-existing file
  ncdf = addfile("clim3D_Amon_MPI-ESM1-2-LR_ssp585_farfuture_tropopause_zonmean.nc" ,"c")  ; open output netCDF file

  ;===================================================================
  ; make time an UNLIMITED dimension; recommended  for most applications
  ;===================================================================
  filedimdef(ncdf,"time",-1,True)

  ;===================================================================
  ; output variables directly; NCL will call appropriate functions
  ; to write the meta data associated with each variable
  ;===================================================================
  ncdf->ptrop  = ptrop
end
