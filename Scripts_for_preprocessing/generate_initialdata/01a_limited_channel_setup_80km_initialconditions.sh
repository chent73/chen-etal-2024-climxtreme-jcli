#!/bin/bash
#=============================================================================
#SBATCH --account=bb1152
#SBATCH --job-name=remap_ini
#SBATCH --partition=compute
#SBATCH --nodes=2
#SBATCH --threads-per-core=2
#SBATCH --mem=120000
#SBATCH --output=/work/bb1152/Module_A/A6_CyclEx/b380490_Albern/input_data/LOG.remap_initial_conditions.%j.o
#SBATCH --error=/work/bb1152/Module_A/A6_CyclEx/b380490_Albern/input_data/LOG.remap_initial_conditions.%j.o
#SBATCH --exclusive
#SBATCH --time=01:00:00
#=========================================================================================
# This script prepares the initial files for a baroclinic life cycle simulation using
# DWD ICONTOOLS.
#=========================================================================================
#-----------------------------------------------------------------------------------------
# Introduce switches to decide what the script will do.
# Switches can be set to "yes" or "no".

# create new folder "inputs_planar_channel_51x81_80km"
makefolder=yes

# run python scripts to generate initial files for control simulation and/or +4K simulation.
# control simulation
ini_python_ctl=yes
# +4K uniform temperature increase, qv consistent with temperature
ini_python_4k=yes
# +4K uniform temperature increase, qv from control simulation
ini_python_4k_qvctl=yes

# run python script to generate the temperature perturbation
tpert_python=yes

# remap initial files from python scripts to channel grid
remap_ini_python_ctl=yes
remap_ini_python_4k=yes
remap_ini_python_4k_qvctl=yes

# remap initial data from IFS to channel grid
remap_ifs=yes

# merge remapped initial files
merge_python_ifs_ctl=yes
merge_python_ifs_4k=yes
merge_python_ifs_4k_qvctl=yes

#-----------------------------------------------------------------------------------------
# Load modules 

module purge
module load anaconda3/bleeding_edge
module load nco/4.7.5-gcc64
module load ncl/6.5.0-gccsys

#--------------------------------------------------------------------------------------
# ICONTOOLS directory

#ICONTOOLS_DIR=/work/bb1018/b380490/icontools-2.1.0/icontools
ICONTOOLS_DIR=/work/bb1152/Module_A/A6_CyclEx/b380490_Albern/icontools-2.1.0/icontools

BINARY_ICONSUB=iconsub_mpi
BINARY_REMAP=iconremap_mpi
BINARY_GRIDGEN=icongridgen

#--------------------------------------------------------------------------------------
# move to folder "input_data"
cd ../../../input_data

#--------------------------------------------------------------------------------------
# file with channel grid

gridfile=Channel_4000x9000_80000m_with_boundary.nc

#cp /work/bb1135/b381185/tools/GridGenerator_master/grids/$gridfile ./

#--------------------------------------------------------------------------------------
# make folder in which the interpolated data will be stored
if [[ "$makefolder" == "yes" ]]; then
    mkdir inputs_planar_channel_51x81_80km
fi

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# 1- Generate initial conditions for baroclinic life cycle 1

# path to python scripts
pypath='../scripts_gitrepo/preprocessing_scripts'

# 1.1 initial conditions
if [[ "$ini_python_ctl" == "yes" ]]; then
    echo "#####################################################"
    echo "Generate initial conditions (CTL)"
    echo "#####################################################"
    # run python script
    python $pypath/lc1_initial_condition_fixedCoriolisParameter_CTL.py
    # extend initial data to 720x360 grid
    cdo remapbil,r720x360 -setgrid,r10x360 lc1_initialcondition_CTL_r10x360.nc lc1_initialcondition_CTL_r720x360.nc
    # remove output from python script
    rm lc1_initialcondition_CTL_r10x360.nc
fi

if [[ "$ini_python_4k" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Generate initial conditions (4K)"
    echo "#####################################################"
    # run python script
    python $pypath/lc1_initial_condition_fixedCoriolisParameter_4K.py
    # extend initial data to 720x360 grid
    cdo remapbil,r720x360 -setgrid,r10x360 lc1_initialcondition_4K_r10x360.nc lc1_initialcondition_4K_r720x360.nc
    # remove output from python script
    rm lc1_initialcondition_4K_r10x360.nc
fi

if [[ "$ini_python_4k_qvctl" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Generate initial conditions (4K, qv from CTL)"
    echo "#####################################################"
    # run python script
    python $pypath/lc1_initial_condition_fixedCoriolisParameter_4K_qvfromcontrol.py
    # extend initial data to 720x360 grid
    cdo remapbil,r720x360 -setgrid,r10x360 lc1_initialcondition_4K_qvCTL_r10x360.nc lc1_initialcondition_4K_qvCTL_r720x360.nc
    # remove output from python script
    rm lc1_initialcondition_4K_qvCTL_r10x360.nc
fi

# 1.2 temperature perturbation
if [[ "$tpert_python" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Generate temperature perturbation"
    echo "#####################################################"
    python $pypath/lc1_perturbation_condition_wavenumber7.py
fi

# 1.3 add temperature perturbation to temperature field
if [[ "$ini_python_ctl" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Merge initial file and temperature perturbation (CTL)"
    echo "#####################################################"
    cdo merge -delvar,T lc1_initialcondition_CTL_r720x360.nc -add -selvar,T lc1_initialcondition_CTL_r720x360.nc lc1_tperturb_720x360.nc lc1_CTL_r720x360.nc
    # remove temporary data
    rm lc1_initialcondition_CTL_r720x360.nc
fi

if [[ "$ini_python_4k" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Merge initial file and temperature perturbation (4K)"
    echo "#####################################################"
    cdo merge -delvar,T lc1_initialcondition_4K_r720x360.nc -add -selvar,T lc1_initialcondition_4K_r720x360.nc lc1_tperturb_720x360.nc lc1_4K_r720x360.nc
    # remove temporary data
    rm lc1_initialcondition_4K_r720x360.nc
fi

if [[ "$ini_python_4k_qvctl" == "yes" ]]; then
    echo ""
    echo "##################################################################"
    echo "Merge initial file and temperature perturbation (4K, qv from CTL)"
    echo "##################################################################"
    cdo merge -delvar,T lc1_initialcondition_4K_qvCTL_r720x360.nc -add -selvar,T lc1_initialcondition_4K_qvCTL_r720x360.nc lc1_tperturb_720x360.nc lc1_4K_qvCTL_r720x360.nc
    # remove temporary data
    rm lc1_initialcondition_4K_qvCTL_r720x360.nc
fi

## remove temperature perturbation file
#rm lc1_tperturb_720x360.nc

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# 2- Remap the lc1 initial file onto the channel grid using DWD ICONTOOLS

cd inputs_planar_channel_51x81_80km

# NOTE: do not use indent for "EOF"; otherwise the calculation terminates with an error
if [[ "$remap_ini_python_ctl" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap initial file (CTL) onto channel grid"
    echo "#####################################################"
    
    for field in  U V W QV QC QI SST LNPS GEOP_SFC GEOP_ML T ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    #cat NAMELIST_ICONREMAP_FIELDS

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = ''
     in_filename       = '../lc1_CTL_r720x360.nc'
     in_type           = 1
     out_grid_filename = '../${gridfile}'
     out_filename      = 'lc1_CTL_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
    #rm -f ../lc1_CTL_r720x360.nc
fi

if [[ "$remap_ini_python_4k" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap initial file (4K) onto channel grid"
    echo "#####################################################"
    
    for field in  U V W QV QC QI SST LNPS GEOP_SFC GEOP_ML T ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    #cat NAMELIST_ICONREMAP_FIELDS

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = ''
     in_filename       = '../lc1_4K_r720x360.nc'
     in_type           = 1
     out_grid_filename = '../${gridfile}'
     out_filename      = 'lc1_4K_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
    #rm -f ../lc1_4K_r720x360.nc
fi

if [[ "$remap_ini_python_4k_qvctl" == "yes" ]]; then
    echo ""
    echo "#######################################################"
    echo "Remap initial file (4K, qv from CTL) onto channel grid"
    echo "#######################################################"

    for field in  U V W QV QC QI SST LNPS GEOP_SFC GEOP_ML T ; do

    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    #cat NAMELIST_ICONREMAP_FIELDS

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = ''
     in_filename       = '../lc1_4K_qvCTL_r720x360.nc'
     in_type           = 1
     out_grid_filename = '../${gridfile}'
     out_filename      = 'lc1_4K_qvCTL_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1

    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
    #rm -f ../lc1_4K_qvCTL_r720x360.nc
fi
 
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
# 3- Remap the initial data from IFS for a zonally-symmetric aquaplanet to
#    the channel grid.

if [[ "$remap_ifs" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap ifs initial file onto channel grid"
    echo "#####################################################"
    
    for field in T_SNOW W_SNOW RHO_SNOW ALB_SNOW SKT STL1 STL2 STL3 STL4 CI W_I Z0 LSM SMIL1 SMIL2 SMIL3 SMIL4 ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = '../inputs_originalgrid/icon_grid_0002_R02B06_G.nc'
     in_filename       = '../inputs_originalgrid/ifs2icon_0002_R02B06_aquaplanet.nc'
     in_type           = 2
     out_grid_filename = '../${gridfile}'
     out_filename      = 'ifs2icon_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
fi

#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
# 4- Construct the complete initial file by adding the remapped output from 
#    the python script and the remapped IFS data.

echo ""
echo "#####################################################"

if [[ "$merge_python_ifs_ctl" == "yes" ]]; then
    echo "Merge remapped initial files (CTL)"

    ncks -v T_SNOW,W_SNOW,RHO_SNOW,ALB_SNOW,SKT,STL1,STL2,STL3,STL4,CI,W_I,Z0,LSM,SMIL1,SMIL2,SMIL3,SMIL4 ifs2icon_remapped.nc temp1.nc

    ncks -A lc1_CTL_remapped.nc temp1.nc

    mv temp1.nc lc1_ifs_CTL_remapped.nc
fi

if [[ "$merge_python_ifs_4k" == "yes" ]]; then
    echo "Merge remapped initial files (4K)"

    ncks -v T_SNOW,W_SNOW,RHO_SNOW,ALB_SNOW,SKT,STL1,STL2,STL3,STL4,CI,W_I,Z0,LSM,SMIL1,SMIL2,SMIL3,SMIL4 ifs2icon_remapped.nc temp1.nc

    ncks -A lc1_4K_remapped.nc temp1.nc

    mv temp1.nc lc1_ifs_4K_remapped.nc
fi

if [[ "$merge_python_ifs_4k_qvctl" == "yes" ]]; then
    echo "Merge remapped initial files (4K, qv from CTL)"

    ncks -v T_SNOW,W_SNOW,RHO_SNOW,ALB_SNOW,SKT,STL1,STL2,STL3,STL4,CI,W_I,Z0,LSM,SMIL1,SMIL2,SMIL3,SMIL4 ifs2icon_remapped.nc temp1.nc

    ncks -A lc1_4K_qvCTL_remapped.nc temp1.nc

    mv temp1.nc lc1_ifs_4K_qvCTL_remapped.nc
fi

#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
# 5- Clean-up: remove temporary files
#if [[ "$remap_ini_python_ctl" == "yes" ]]; then
#    rm lc1_CTL_remapped.nc
#fi
#
#if [[ "$remap_ini_python_4k" == "yes" ]]; then
#    rm lc1_4K_remapped.nc
#fi
#
#if [[ "$remap_ini_python_4k_qvctl" == "yes" ]]; then
#    rm lc1_4K_qvCTL_remapped.nc
#fi
#
#if [[ "$remap_ifs" == "yes" ]]; then
#    rm ifs2icon_remapped.nc
#fi

#
