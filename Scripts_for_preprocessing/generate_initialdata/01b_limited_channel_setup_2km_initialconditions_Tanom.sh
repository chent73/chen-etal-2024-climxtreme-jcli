#!/bin/bash
#=============================================================================
#SBATCH --account=bb1152
#SBATCH --job-name=remap_ini
#SBATCH --partition=compute
#SBATCH --nodes=2
#SBATCH --threads-per-core=2
#SBATCH --mem=120000
#SBATCH --output=/work/bb1152/Module_A/A6_CyclEx/b380490_Albern/input_data/LOG.remap_initial_conditions_tanom.%j.o
#SBATCH --error=/work/bb1152/Module_A/A6_CyclEx/b380490_Albern/input_data/LOG.remap_initial_conditions_tanom.%j.o
#SBATCH --exclusive
#SBATCH --time=01:00:00
#=========================================================================================
# This script prepares the initial files for a baroclinic life cycle simulation using
# DWD ICONTOOLS.
#=========================================================================================
#-----------------------------------------------------------------------------------------
# Introduce switches to decide what the script will do.
# Switches can be set to "yes" or "no".

# run python scripts to generate initial files with temperature anomalies from CMIP6 data
# "global" temperature anomaly
ini_python_tanom=no#yes
# temperature anomaly in tropics
ini_python_tropic=no#yes
# temperature anomaly in polar region
ini_python_polar=no#yes

# path to files with temperature anomalies
ipath_tanom='/work/bb1152/Module_A/A6_CyclEx/b380490_Albern/CMIP6_anomalies_plev/'
# file that contains the temperature anomalies
ifile_tanom='MPI-ESM1-2-LR_farfuture-historical_zm.nc'

# run python script to generate the temperature perturbation
tpert_python=no#yes

# remap initial files from python scripts to channel grid
remap_ini_python_tanom=yes
remap_ini_python_tropic=yes
remap_ini_python_polar=yes

# remap initial data from IFS to channel grid
remap_ifs=no#yes

# merge remapped initial files
merge_python_ifs_tanom=yes
merge_python_ifs_tropic=yes
merge_python_ifs_polar=yes

#-----------------------------------------------------------------------------------------
# Load modules 

module purge
module load anaconda3/bleeding_edge
module load nco/4.7.5-gcc64
module load ncl/6.5.0-gccsys

#--------------------------------------------------------------------------------------
# ICONTOOLS directory

#ICONTOOLS_DIR=/work/bb1018/b380490/icontools-2.1.0/icontools
ICONTOOLS_DIR=/work/bb1152/Module_A/A6_CyclEx/b380490_Albern/icontools-2.1.0/icontools

BINARY_ICONSUB=iconsub_mpi
BINARY_REMAP=iconremap_mpi
BINARY_GRIDGEN=icongridgen

#--------------------------------------------------------------------------------------
# move to folder "input_data"
cd ../../../input_data

#--------------------------------------------------------------------------------------
# file with channel grid

gridfile=Channel_4000x9000_2500m_with_boundary.nc

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# 1- Generate initial conditions for baroclinic life cycle 1

# path to python scripts
pypath='../scripts_gitrepo/preprocessing_scripts'

# 1.1 initial conditions
if [[ "$ini_python_tanom" == "yes" ]]; then
    echo "#####################################################"
    echo "Generate initial conditions (tanom)"
    echo "#####################################################"
    # run python script
    python $pypath/lc1_initial_condition_fixedCoriolisParameter_Tanom.py $ipath_tanom $ifile_tanom
    # extend initial data to 720x360 grid
    cdo remapbil,r720x360 -setgrid,r10x360 lc1_initialcondition_Tanom_r10x360.nc lc1_initialcondition_Tanom_r720x360.nc
    # remove output from python script
    rm lc1_initialcondition_Tanom_r10x360.nc
fi

if [[ "$ini_python_tropic" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Generate initial conditions (tanom tropics)"
    echo "#####################################################"
    # run python script
    python $pypath/lc1_initial_condition_fixedCoriolisParameter_Tanom_TR_PO.py $ipath_tanom $ifile_tanom $"tropical"
    # extend initial data to 720x360 grid
    cdo remapbil,r720x360 -setgrid,r10x360 lc1_initialcondition_Tanom_tropics_r10x360.nc lc1_initialcondition_Tanom_tropics_r720x360.nc
    # remove output from python script
    rm lc1_initialcondition_Tanom_tropics_r10x360.nc
fi

if [[ "$ini_python_polar" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Generate initial conditions (tanom polar)"
    echo "#####################################################"
    # run python script
    python $pypath/lc1_initial_condition_fixedCoriolisParameter_Tanom_TR_PO.py $ipath_tanom $ifile_tanom $"polar"
    # extend initial data to 720x360 grid
    cdo remapbil,r720x360 -setgrid,r10x360 lc1_initialcondition_Tanom_polar_r10x360.nc lc1_initialcondition_Tanom_polar_r720x360.nc
    # remove output from python script
    rm lc1_initialcondition_Tanom_polar_r10x360.nc
fi

# 1.2 temperature perturbation
if [[ "$tpert_python" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Generate temperature perturbation"
    echo "#####################################################"
    python $pypath/lc1_perturbation_condition_wavenumber7.py
fi

# 1.3 add temperature perturbation to temperature field
if [[ "$ini_python_tanom" == "yes" ]]; then
    echo ""
    echo "###############################################################"
    echo "Merge initial file and temperature perturbation (tanom)"
    echo "###############################################################"
    cdo merge -delvar,T lc1_initialcondition_Tanom_r720x360.nc -add -selvar,T lc1_initialcondition_Tanom_r720x360.nc lc1_tperturb_720x360.nc lc1_Tanom_r720x360.nc
    # remove temporary data
    rm lc1_initialcondition_Tanom_r720x360.nc
fi

if [[ "$ini_python_tropic" == "yes" ]]; then
    echo ""
    echo "###############################################################"
    echo "Merge initial file and temperature perturbation (tanom tropic)"
    echo "###############################################################"
    cdo merge -delvar,T lc1_initialcondition_Tanom_tropics_r720x360.nc -add -selvar,T lc1_initialcondition_Tanom_tropics_r720x360.nc lc1_tperturb_720x360.nc lc1_Tanom_tropics_r720x360.nc
    # remove temporary data
    rm lc1_initialcondition_Tanom_tropics_r720x360.nc
fi

if [[ "$ini_python_polar" == "yes" ]]; then
    echo ""
    echo "###############################################################"
    echo "Merge initial file and temperature perturbation (tanom polar)"
    echo "###############################################################"
    cdo merge -delvar,T lc1_initialcondition_Tanom_polar_r720x360.nc -add -selvar,T lc1_initialcondition_Tanom_polar_r720x360.nc lc1_tperturb_720x360.nc lc1_Tanom_polar_r720x360.nc
    # remove temporary data
    rm lc1_initialcondition_Tanom_polar_r720x360.nc
fi

## remove temperature perturbation file
#rm lc1_tperturb_720x360.nc

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# 2- Remap the lc1 initial file onto the channel grid using DWD ICONTOOLS

cd inputs_planar_channel_51x81_2km

# NOTE: do not use indent for "EOF"; otherwise the calculation terminates with an error
if [[ "$remap_ini_python_tanom" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap initial file (tanom) onto channel grid"
    echo "#####################################################"
    
    for field in  U V W QV QC QI SST LNPS GEOP_SFC GEOP_ML T ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    #cat NAMELIST_ICONREMAP_FIELDS

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = ''
     in_filename       = '../lc1_Tanom_r720x360.nc'
     in_type           = 1
     out_grid_filename = '../${gridfile}'
     out_filename      = 'lc1_Tanom_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
    #rm -f ../lc1_Tanom_r720x360.nc
fi

if [[ "$remap_ini_python_tropic" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap initial file (tanom tropic) onto channel grid"
    echo "#####################################################"
    
    for field in  U V W QV QC QI SST LNPS GEOP_SFC GEOP_ML T ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    #cat NAMELIST_ICONREMAP_FIELDS

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = ''
     in_filename       = '../lc1_Tanom_tropics_r720x360.nc'
     in_type           = 1
     out_grid_filename = '../${gridfile}'
     out_filename      = 'lc1_Tanom_tropics_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
    #rm -f ../lc1_Tanom_tropics_r720x360.nc
fi

if [[ "$remap_ini_python_polar" == "yes" ]]; then
    echo ""
    echo "#######################################################"
    echo "Remap initial file (tanom polar) onto channel grid"
    echo "#######################################################"

    for field in  U V W QV QC QI SST LNPS GEOP_SFC GEOP_ML T ; do

    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    #cat NAMELIST_ICONREMAP_FIELDS

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = ''
     in_filename       = '../lc1_Tanom_polar_r720x360.nc'
     in_type           = 1
     out_grid_filename = '../${gridfile}'
     out_filename      = 'lc1_Tanom_polar_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1

    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
    #rm -f ../lc1_Tanom_polar_r720x360.nc
fi
 
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
# 3- Remap the initial data from IFS for a zonally-symmetric aquaplanet to
#    the channel grid.

if [[ "$remap_ifs" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap ifs initial file onto channel grid"
    echo "#####################################################"
    
    for field in T_SNOW W_SNOW RHO_SNOW ALB_SNOW SKT STL1 STL2 STL3 STL4 CI W_I Z0 LSM SMIL1 SMIL2 SMIL3 SMIL4 ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = '../inputs_originalgrid/icon_grid_0002_R02B06_G.nc'
     in_filename       = '../inputs_originalgrid/ifs2icon_0002_R02B06_aquaplanet.nc'
     in_type           = 2
     out_grid_filename = '../${gridfile}'
     out_filename      = 'ifs2icon_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
fi

#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
# 4- Construct the complete initial file by adding the remapped output from 
#    the python script and the remapped IFS data.

echo ""
echo "#####################################################"

if [[ "$merge_python_ifs_tanom" == "yes" ]]; then
    echo "Merge remapped initial files (tanom)"

    ncks -v T_SNOW,W_SNOW,RHO_SNOW,ALB_SNOW,SKT,STL1,STL2,STL3,STL4,CI,W_I,Z0,LSM,SMIL1,SMIL2,SMIL3,SMIL4 ifs2icon_remapped.nc temp1.nc

    ncks -A lc1_Tanom_remapped.nc temp1.nc

    mv temp1.nc lc1_ifs_Tanom_remapped.nc
fi

if [[ "$merge_python_ifs_tropic" == "yes" ]]; then
    echo "Merge remapped initial files (tanom tropic)"

    ncks -v T_SNOW,W_SNOW,RHO_SNOW,ALB_SNOW,SKT,STL1,STL2,STL3,STL4,CI,W_I,Z0,LSM,SMIL1,SMIL2,SMIL3,SMIL4 ifs2icon_remapped.nc temp1.nc

    ncks -A lc1_Tanom_tropics_remapped.nc temp1.nc

    mv temp1.nc lc1_ifs_Tanom_tropics_remapped.nc
fi

if [[ "$merge_python_ifs_polar" == "yes" ]]; then
    echo "Merge remapped initial files (tanom polar)"

    ncks -v T_SNOW,W_SNOW,RHO_SNOW,ALB_SNOW,SKT,STL1,STL2,STL3,STL4,CI,W_I,Z0,LSM,SMIL1,SMIL2,SMIL3,SMIL4 ifs2icon_remapped.nc temp1.nc

    ncks -A lc1_Tanom_polar_remapped.nc temp1.nc

    mv temp1.nc lc1_ifs_Tanom_polar_remapped.nc
fi

#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
# 5- Clean-up: remove temporary files
#if [[ "$remap_ini_python_tanom" == "yes" ]]; then
#    rm lc1_Tanom_remapped.nc
#fi
#
#if [[ "$remap_ini_python_tropic" == "yes" ]]; then
#    rm lc1_Tanom_tropics_remapped.nc
#fi
#
#if [[ "$remap_ini_python_polar" == "yes" ]]; then
#    rm lc1_Tanom_polar_remapped.nc
#fi
#
#if [[ "$remap_ifs" == "yes" ]]; then
#    rm ifs2icon_remapped.nc
#fi

#
