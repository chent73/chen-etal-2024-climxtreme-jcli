import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt
import netCDF4 as nc
from numba import jit
import xarray as xr # for temperature anomalies
import sys          # for temperature anomalies

@jit
def Tintegrand(latrad, z, zT, U0, a, Omega):
    f  = 2*Omega*np.sin(np.deg2rad(45.0))
    F  = np.power(np.sin(np.pi*np.power(np.sin(latrad),2)),3)
    if latrad<0: F=0.0
    u1 = U0*F*(z/zT)*np.exp(-0.5*(np.power(z/zT,2)-1))
    du1dz = u1*(1/z-z/np.power(zT,2))
    return (a*f+2*u1*np.tan(latrad))*du1dz

def load_levelinfo():
    from netCDF4 import Dataset
    file = Dataset('./ifs2icon_verticalgridinfo_137levels.nc', 'r')
    hyam = np.squeeze(np.array(file.variables['hyam']))
    hybm = np.squeeze(np.array(file.variables['hybm']))   
    hyai = np.squeeze(np.array(file.variables['hyai']))
    hybi = np.squeeze(np.array(file.variables['hybi']))   
    lev  = np.squeeze(np.array(file.variables['lev' ]))
    lev_2= np.squeeze(np.array(file.variables['lev_2']))    
    return hyam, hybm, hyai, hybi, lev, lev_2
    
hyam, hybm, hyai, hybi, lev, lev_2 = load_levelinfo()

# function to calculate the meridional gradient of variable "var"
def get_dxdlat(var, lats, levs):
    nlat = lats.size # length of latitude vector
    # calculate distance between entries of latitude vector
    dlat = np.full(lats.shape, np.nan, dtype=float)
    dlat[0] = np.abs(lats[1] - lats[0])
    # distance between lat[2]-lat[0], lat[3]-lat[1], etc.
    dlat[1:nlat-1] = np.abs(lats[2:nlat] - lats[0:nlat-2])
    dlat[nlat-1] = np.abs(lats[nlat-1] - lats[nlat-2])

    # check that first dimension is levs and second dimension is lats
    if (var.shape[0] == levs.size) and (var.shape[1] == lats.size):
        # check that latitudes go from south to north
        if lats[0] > lats[1]:
            var = var[:, ::-1]
            lats = lats[::-1]
            print('changed lats and var')

        # centered finite differences
        var_grad = np.full(var.shape, np.nan, dtype=float)
        var_grad[:, 0] = (var[:, 1] - var[:, 0]) / dlat[0]
        for la in range(1, nlat-1):
            var_grad[:, la] = (var[:, la+1] - var[:, la-1]) / dlat[la]
        del la
        var_grad[:, nlat-1] = (var[:, nlat-1] - var[:, nlat-2]) / dlat[nlat-1]
    else:
        print('ERROR: Dimensions are not lat and lev. ' + \
              'Exit function get_dxdlat')
        return

    return var_grad

# function to calculate the zonal wind from atm. temperature following thermal
# wind balance
# only the tropical or polar temperature anomaly is used to derive the zonal
# wind anomaly
def get_u_from_t_trpo(tanom, tropo, zin, zout, lat, latrad, trpo):
# input: tanom: atmospheric temperature anomaly, dimensions (zin-lat)
#        tropo: tropopause height in m
#        zin: vertical levels of temperature field in m
#        zout: vertical levels of zonal wind field in m
#        lat: latitudes of temperature field in degree
#        latrad: latitudes in radian
#        trpo: use tropical or polar temperature anomaly

    # 1. prepare the temperature field
    # 1.1 We want to derive the zonal wind from the surface to TOA.
    #     If levels do not go from surface to TOA, change them.
    if zin[0] > zin[1]:
        print('change order of zin to go from surface to TOA')
        tanom_calc = tanom[::-1, :]
        zin_calc = zin[::-1]
    else:
        tanom_calc = tanom.copy()
        zin_calc = zin.copy()
    del tanom, zin

    # 1.2 interpolate the vertical grid to a finer resolution
    #     (10m distance between levels), use height levels zout that
    #     come from the initial data
    if zout[0] > zout[1]:
        print('zout from TOA to surface')
        zin_int = np.arange(zout[-1], zout[0], 10)
    else:
        print('zout from surface to TOA')
        zin_int = np.arange(zout[0], zout[-1], 10)
    tanom_int = np.full((zin_int.size, lat.size), np.nan, dtype=float)
    for la in range(lat.size): # loop over latitudes
        tanom_int[:, la] = np.interp(zin_int, zin_calc, tanom_calc[:,la])
    del la
    del tanom_calc, zin_calc

    # 1.3 set temperature anomalies in stratosphere to 0 and smooth the temperature
    #     field around the tropopause to avoid sharp gradients
    mask = np.full(tanom_int.shape, np.nan, dtype=bool)
    for la in range(len(lat)): # loop over latitudes
        mask[:,la] = zin_int < tropo[la]
    del la
    # smooth data around the tropopause
    tanom_mask = tanom_int * mask
    for la in range(len(lat)): # loop over latitudes
        # index of highest level below tropopause
        ind = np.where(mask[:,la])[0][-1]
        # smooth data for 25 levels below and 24 levels above tropopause
        x = np.linspace(tanom_int[ind-25, la], 0, 50)
        tanom_mask[ind-25:ind+25, la] = x
        del ind, x
    del la

    del tanom_int, mask, tropo

    # 1.4 smooth the temperature profile
    window = np.ones(10)/10
    tanom_mask_runmean = np.full(tanom_mask.shape, np.nan, dtype=float)
    for le in range(len(zin_int)):
        tanom_mask_runmean[le, :] = np.convolve(tanom_mask[le, :], window, 'same')
    del le, window

    del tanom_mask

    # the running mean introduces strong gradients at the North pole
    # -> set temperature values poleward of 85N to the value at 85N
    xlat = (np.abs(lat-85)).argmin()
    for la in range(xlat+1, len(lat)):
        tanom_mask_runmean[:, la] = tanom_mask_runmean[:, xlat]
    del la
    del xlat
    
    # 1.5 apply mask to get tropical or polar temperature anomalies
    latsout = (np.abs(lat - 40)).argmin()  
    latnort = (np.abs(lat - 50)).argmin()+2
    print(lat[latsout:latnort])
    mask_trpo = np.full(lat.shape, np.nan, dtype=float)
    if trpo == 'tropical':
        trop_lin = np.linspace(1, 0, 22)
        mask_trpo[:latsout] = 1
        mask_trpo[latsout:latnort] = trop_lin
        mask_trpo[latnort:] = 0
        del trop_lin
    elif trpo == 'polar':
        polar_lin = np.linspace(0, 1, 22)
        mask_trpo[:latsout] = 0
        mask_trpo[latsout:latnort] = polar_lin
        mask_trpo[latnort:] = 1
        del polar_lin
    else:
        print('trpo must be "tropical" or "polar"')
        mask_trpo = 1
    del latsout, latnort
    
    # apply mask to tanom_mask_runmean
    tanom_mask_runmean = tanom_mask_runmean * mask_trpo
    del mask_trpo

    ###################################################################
    # 2. calculate the zonal wind field
    # 2.1 helper variables
    f = 2*Omega*np.sin(np.deg2rad(45.0))
    var_a = -1*H*a*f/R
    var_b = -2*H*np.tan(latrad)/R

    # 2.2 meridional temperature gradient
    dTdlat = get_dxdlat(tanom_mask_runmean, latrad, zin_int)

    # smooth the meridional temperature gradient
    window = np.ones(20)/20
    dTdlat_runmean = np.full(dTdlat.shape, np.nan, dtype=float)
    for le in range(len(zin_int)):
        dTdlat_runmean[le, :] = np.convolve(dTdlat[le, :], window, 'same')
    del le, window
    del dTdlat

    # 2.3 difference between levels
    #     array has one entry less than z: first entry is
    #     the difference between the first and second level
    dz = np.diff(zin_int)

    # 2.4 calculate zonal wind
    uanom_int = np.full(tanom_mask_runmean.shape, np.nan, dtype=float)

    # no wind at the surface
    uanom_int[0, :] = 0.0

    # other levels
    for le in range(1, len(zin_int)): # loop over levels
        uanom_int[le, :] = (dTdlat_runmean[le-1,:] + \
                            var_a * uanom_int[le-1,:] / dz[le-1] + \
                            var_b * uanom_int[le-1,:] * uanom_int[le-1,:] / dz[le-1]) * \
                           dz[le-1] / (var_a + var_b * uanom_int[le-1,:])
    del le
    del f, var_a, var_b, dTdlat_runmean, dz

    # 2.5 interpolate the calculated zonal wind and the temperature anomaly
    #     to the zout vertical grid
    uanom = np.full((zout.size, lat.size), np.nan, dtype=float)
    tanom = np.full((zout.size, lat.size), np.nan, dtype=float)

    # levels must be monotonically increasing:
    if zout[0] > zout[1]:
        print('zout is not monotonically increasing. Use zout[::-1].')
        for la in range(lat.size):
            uanom[:, la] = np.interp(zout[::-1], zin_int, uanom_int[:, la])
            tanom[:, la] = np.interp(zout[::-1], zin_int, tanom_mask_runmean[:, la])
        del la
        del zin_int, uanom_int, tanom_mask_runmean

        # first level should be TOA again
        tanom = tanom[::-1, :]
        uanom = uanom[::-1, :]
    else:
        print('zout is monotonically increasing. Use z.')
        for la in range(lat.size):
            uanom[:, la] = np.interp(zout, zin_int, uanom_int[:, la])
            tanom[:, la] = np.interp(zout, zin_int, tanom_mask_runmean[:, la])
        del la
        del zin_int, uanom_int, tanom_mask_runmean

        # first level should be TOA again
        tanom = tanom[::-1, :]
        uanom = uanom[::-1, :]

    return uanom, tanom

# constants
# physical constants are set to the values used in icon-nwp-2.0.15/src/shared/mo_physical_constants.f90
u0     = 45.0           # in m/s
zT     = 13.0e3         # in m
H      = 7.5e3          # in m
R      = 287.04         # dry gas constant in J/(kg K) (parameter rd in ICON)
a      = 6.371229e6     # average Earth radius in m (parameter earth_radius in ICON)
Omega  = 7.29212e-5     # angular velocity in 1/s (parameter earth_angular_velocity in ICON) 
t0     = 300            # in K
Gamma0 = -6.5e-3        # in K/m
alpha  = 10             # unitless
kappa  = 2.0/7.0        # unitless
g      = 9.80665        # av. gravitational acceleration in m/s2 (parameter grav in ICON)
p0     = 1.0e5          # globally-uniform surface pressure in Pa
# for relative humidity following Booth et al., 2013 Climate Dynamics
zTrh   = 12.0e3        
rh0    = 0.80           # relative humidity scaling factor from 0..1

#latitude-longitude grid
lat  = np.arange(-89.75, 90, 0.5) #np.linspace(-90, 90, 360)
lon  = np.linspace(0.0,360,10)
nlat = lat.size
nlon = lon.size

# vertical grid: for computation of initial state we convert the ifs2icon hybrid levels
# to height levels assuming a globally-uniform surface pressure (defined above)
# and defining ehight according to Polvani and Elsner as z = H ln (p0/p)
p  = hyam + hybm*p0
z  = H*np.log(p0/p)  # np.log is natural logarithm
nz   = z.size
#print(z, nz)

# latitude in radians
latrad = lat * np.pi/180.0 #lat2

# lifecycle 1
u1 = np.zeros((nz,nlat))+np.nan
F  = np.power(np.sin(np.pi*np.power(np.sin(latrad),2)),3); F[lat<0] = 0.0
for i in range(0, nz):
    for j in range(0, nlat):
        u1[i,j] = u0*F[j]*(z[i]/zT)*np.exp(-0.5*(np.power(z[i]/zT,2)-1))
#du1dz = u1*np.expand_dims(1/z-z/np.power(zT,2),axis=1)

# compute temperature profile in zonal wind balance with u1
t=np.zeros((nz, nlat)) + np.nan

# latitude independe reference profile
tr = np.zeros((nz, nlat)) + np.nan
for i in range(0, nz):
    tr[i, :] = t0 + Gamma0/np.power((np.power(zT,-alpha)+np.power(z[i],-alpha)),1/alpha)

# latitude dependent modification
tmp = np.zeros((nz, nlat)) + np.nan
for i in range(0, nz):
    for j in range(0, nlat):
        tmp[i, j] =scipy.integrate.quad(Tintegrand, 0, latrad[j], args=(z[i], zT, u0, a, Omega))[0]
t=tr-H/R*tmp

#######################################################################
# BEGINNING OF CALCULATIONS FOR TEMPERATURE AND ZONAL WIND ANOMALIES
# read the temperature anomaly from CMIP6 models and derive the zonal
# wind anomaly for this temperature anomaly based on thermal wind
# balance

# read the temperature anomaly
# the path and filename are given in the bash script that calls
# this python script and are read in here with "sys"
ipath = sys.argv[1]
ifile = sys.argv[2]
da_tanom = xr.open_dataset(ipath + ifile).squeeze()['ta']
del ipath, ifile

# store values for tanom and latitude in numpy arrays
tanom = da_tanom.values
lat_tanom = da_tanom['lat'].values

# convert pressure levels to height levels in m
# as in Polvani & Esler 2007
zlev_tanom = (H*np.log(p0/da_tanom['plev'])).values
del da_tanom

# Check if the temperature anomaly is nan for the first index (in latitude).
# If true: set value at first index to value at second index
if np.isnan(tanom[:,0]).all():
    print('change column for first latitude of tanom')
    tanom[:,0] = tanom[:,1]
print("Nan's in tanom: " + str(np.isnan(tanom).any()))

# set temperature anomalies southward of 20N to value at 20N
# and northward of 80N to value at 80N to avoid sharp gradients
tanom_alllat = np.full((zlev_tanom.size, lat.size), np.nan, dtype=float)

# find indices, where lat agrees with the first and last entries of lat_tanom
xlat = np.where(lat == lat_tanom[0])[0][0]
ylat = np.where(lat == lat_tanom[-1])[0][0]

tanom_alllat[:, xlat:ylat+1] = tanom
for la in range(len(lat[:xlat])):
    tanom_alllat[:, la] = tanom[:, 0]
del la
for la in range(len(lat[ylat+1:])):
    tanom_alllat[:, la+ylat+1] = tanom[:, -1]
del xlat, ylat
del tanom, lat_tanom

#######################################################################
# read tropopause for initial conditions
ipath = '/work/bb1152/Module_A/A6_CyclEx/b380490_Albern/input_data/' + \
        'lc1_initialcondition_tropopause/'
ifile = 'lc1_initialcondition_CTL_tropopause_zm.nc'
tropo_ini_plev = xr.open_dataset(ipath + ifile)
del ipath, ifile

# convert tropopause to height in m
tropo_ini = (H*np.log(p0/tropo_ini_plev['ptrop'])).values
del tropo_ini_plev

#######################################################################
# calculate zonal wind anomaly based on temperature anomaly

# decide if tropical or polar temperature anomaly is used
# as the path and filename for the temperature anomaly, this variable
# is given in the bash script that calls this python script and is
# read in here with "sys"
trpo = sys.argv[3]
print('type of trpo: ', type(trpo))

uanom, tanom_smooth = get_u_from_t_trpo(tanom_alllat, tropo_ini,
                                        zlev_tanom, z, lat, latrad,
                                        trpo)

#######################################################################
# add temperature and temperature anomaly
t = t + tanom_smooth

# add zonal wind and zonal wind anomaly
u1 = u1 + uanom 

#######################################################################
# delete auxiliary variables
del zlev_tanom, tanom_alllat, tropo_ini, uanom, tanom_smooth

# END OF CALCULATIONS FOR TEMPERATURE AND ZONAL WIND ANOMALY
#######################################################################

# potential temperature
theta=t*np.expand_dims(np.exp(kappa*z/H),axis=1)
   
# relative humidity
# follows Booth et al. 2013, Climate Dynamics 
rh = np.zeros((nz, nlat)) + np.nan
for i in range(0, nz):
    rh[i, :] = rh0*np.power(1-0.85*z[i]/zTrh, 1.25)
    if z[i]>14e3:
        rh[i, :] = 0.0

# specific humidty
# follows calculation in icon 
# /icon-nwp-2.0.15/src/atm_phy_schemes/mo_satad.f90 -> sat_pres_water,spec_humi
b1 = 610.78  # --> c1es in mo_convect_tables.f90 
b2w= 17.269  # --> c3les 
b3 = 273.15  # --> tmelt; melting temperature in K
b4w= 35.86   # --> c4les
sat_pres_water = b1*np.exp(b2w*(t-b3)/(t-b4w))
Rdv = 287.04/461.51   # Rd/Rv
o_m_Rdv = 1-Rdv       # 1-Rd/Rv
print(Rdv, o_m_Rdv)  
qv = np.zeros((nz, nlat))
for i in range(0, nz):
    for j in range(0, nlat):
        qv[i, j] = rh[i,j]*Rdv*sat_pres_water[i,j]/(p[i]-o_m_Rdv*sat_pres_water[i,j])

#---------------------------------------
# save to netcdf files
#---------------------------------------
if trpo == 'tropical':
    ncfile = nc.Dataset('lc1_initialcondition_Tanom_tropics_r10x360.nc', 'w', clobber=True, format='NETCDF3_CLASSIC')
elif trpo == 'polar':
    ncfile = nc.Dataset('lc1_initialcondition_Tanom_polar_r10x360.nc', 'w', clobber=True, format='NETCDF3_CLASSIC')
ncfile.description = 'Initial condition for LC1, for U and T according to Polvani and Esler 2007 JGR'
 
# dimensions
ncfile.createDimension('lat', nlat)
ncfile.createDimension('lon', nlon)
ncfile.createDimension('lev', nz)
ncfile.createDimension('lev_2', 1)
ncfile.createDimension('nhym', nz)
ncfile.createDimension('nhyi', nz+1)
ncfile.createDimension('time', 1)

# variables
nc_latitude  = ncfile.createVariable('lat'     , 'f8', ('lat',))
nc_longitude = ncfile.createVariable('lon'     , 'f8', ('lon',))
nc_lev       = ncfile.createVariable('lev'     , 'f8', ('lev',))
nc_lev_2     = ncfile.createVariable('lev_2'   , 'f8', ('lev_2',))
nc_hyam      = ncfile.createVariable('hyam'    , 'f8', ('nhym',))
nc_hybm      = ncfile.createVariable('hybm'    , 'f8', ('nhym',))
nc_hyai      = ncfile.createVariable('hyai'    , 'f8', ('nhyi',))
nc_hybi      = ncfile.createVariable('hybi'    , 'f8', ('nhyi',))
nc_time      = ncfile.createVariable('time'    , 'f8', ('time',))
nc_T         = ncfile.createVariable('T'       , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_U         = ncfile.createVariable('U'       , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_V         = ncfile.createVariable('V'       , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_W         = ncfile.createVariable('W'       , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_QV        = ncfile.createVariable('QV'      , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_QC        = ncfile.createVariable('QC'      , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_QI        = ncfile.createVariable('QI'      , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_SST       = ncfile.createVariable('SST'     , 'f4', ('time', 'lat', 'lon'))   
nc_LNPS      = ncfile.createVariable('LNPS'    , 'f4', ('time', 'lev_2', 'lat', 'lon'))
nc_GEOP_SFC  = ncfile.createVariable('GEOP_SFC', 'f4', ('time', 'lev_2', 'lat', 'lon'))
nc_GEOP_ML   = ncfile.createVariable('GEOP_ML' , 'f4', ('time', 'lev_2', 'lat', 'lon'))
  
# set horizontal grid data
nc_latitude[:]  = lat
nc_longitude[:] = lon

# set vertical grid data
nc_lev[:]       = lev
nc_lev.standard_name = "hybrid_sigma_pressure"
nc_lev.long_name = "hybrid level at layer midpoints"
nc_lev.formula = "hyam hybm (mlev=hyam+hybm*aps)"
nc_lev.formula_terms = "ap: hyam b: hybm ps: aps"
nc_lev.units = "level"
nc_lev.positive = "down"
nc_lev_2[:]       = lev_2
nc_lev_2.standard_name = "hybrid_sigma_pressure"
nc_lev_2.long_name = "hybrid level at layer midpoints"
nc_lev_2.formula = "hyam hybm (mlev=hyam+hybm*aps)"
nc_lev_2.formula_terms = "ap: hyam b: hybm ps: aps"
nc_lev_2.units = "level"
nc_lev_2.positive = "down"
nc_hyam[:] = hyam
nc_hyam.long_name = "hybrid A coefficient at layer midpoints"
nc_hyam.units = "Pa"
nc_hybm[:] = hybm
nc_hybm.long_name = "hybrid B coefficient at layer midpoints"
nc_hybm.units = "1"
nc_hyai[:] = hyai
nc_hyai.long_name = "hybrid A coefficient at layer interfaces"
nc_hyai.units = "Pa"
nc_hybi[:] = hybi
nc_hybi.long_name = "hybrid B coefficient at layer interfaces"
nc_hybi.units = "1"

# set time data
nc_time[0] = 23790716.25 # this is the time from ifs2icon_0010_R02B04_aquaplanet.nc
nc_time.standard_name = "time"
nc_time.units = "day as %Y%m%d.%f"
nc_time.calendar = "proleptic_gregorian"
nc_time.axis = "T"

# set variable data
for i in range(0, nlon):
    nc_T[:,:,:,i]  = t
    nc_U[:,:,:,i]  = u1
    nc_V[:,:,:,i]  = 0.0
    nc_W[:,:,:,i]  = 0.0
    nc_QV[:,:,:,i] = qv
    nc_QC[:,:,:,i] = 0.0
    nc_QI[:,:,:,i] = 0.0
    nc_SST[:,:,i]         = t[nz-1,:] - 0.5 # as in Booth et al., 2013, Clim. Dynamics we set the SST to lowest-level initial T-0.5K
    nc_LNPS[:,:,:,i]      = np.log(p0)
    nc_GEOP_SFC[:,:,:,i]  = 0.0
    nc_GEOP_ML[:,:,:,i]   = g*z[nz-1]

# set variable attributes
nc_T.units = "K"; nc_T.standard_name = "temperature"; nc_T.long_name = "Atmospheric temperature"
nc_U.units = "m s**-1"; nc_U.standard_name = "u-wind"; nc_U.long_name = "Zonal wind"
nc_V.units = "m s**-1"; nc_V.standard_name = "v-wind"; nc_V.long_name = "Meridional wind"
nc_W.units = "Pa s**-1"; nc_W.standard_name = "lagrangian_tendency_of_air_pressure"; nc_W.long_name = "Vertical Velocity (Pressure) (omega=dp/dt)"
nc_QV.units = "kg kg**-1"; nc_QV.standard_name = "spec. humidity"; nc_QV.long_name = "Specific humidity"
nc_QC.units = "kg kg**-1"; nc_QC.standard_name = "cloud liq. water"; nc_QC.long_name = "Specific cloud liquid water content"
nc_QI.units = "kg kg**-1"; nc_QI.standard_name = "cloud ice"; nc_QI.long_name = "Specific cloud ice water content"
nc_GEOP_ML.units = "m**2 s**-2"; nc_GEOP_ML.standard_name = "geopotential"; nc_GEOP_ML.long_name = "Geopotential"
nc_SST.units = "K"; nc_SST.standard_name = "sst"; nc_SST.long_name = "Sea-surface temperature"
nc_LNPS.units = "n/a"; nc_LNPS.standard_name = "ln sfc pressure"; nc_LNPS.long_name = "Logarithm of surface pressure"
nc_GEOP_SFC.units = "m**2 s**-2"; nc_GEOP_SFC.standard_name = "sfc geopotential"; nc_GEOP_SFC.long_name = "Sfc Geopotential"
 
ncfile.close()
