#!/bin/bash
# Prepare 6-hourly model output on hybrid sigma levels for ICON simulation
# historical:  1980-2010 -> winter 1980/81 - 2009/10
# near future: 2020-2050 -> winter 2020/21 - 2049/50
# far future:  2070-2100 -> winter 2070/71 - 2099/2100

institute="MPI-M"
model="MPI-ESM1-2-LR"

# height levels from ICON initial conditions file
z="86345.5722915056,79322.4093339252,76170.1357211878,73231.8163420052,70486.3538673693,67915.2997410308,65502.4537494155,63233.5368503841,61095.9178385572,59078.3837407794,57170.9495323468,55364.6954914310,53651.6294395941,52024.5710477045,50477.0516284331,49003.2296016428,47597.8157372562,46256.0105986010,44973.4495802030,43746.1541951765,42570.4912757526,41443.1365116378,40361.0425635773,39321.4107735892,38321.6659695432,37359.4352149795,36432.5285699321,35538.9216273151,34676.7397702895,33844.2451757581,33039.8250383766,32261.9801857344,31509.3142704131,30780.5259960997,30074.4019263420,29389.8078588518,28725.6832619181,28081.0353174911,27454.9329702988,26846.5033455732,26254.9266416751,25679.4317675026,25119.2933299999,24573.8280085659,24042.4566394904,23524.7537974728,23020.3679085019,22528.9558286229,22050.1844356583,21583.7290375909,21129.1227772948,20685.3989775932,20251.2752075614,19825.5353714588,19407.0497385363,18994.7634584440,18587.6682754168,18184.7940552324,17785.4424700615,17389.3842191586,16996.6080444898,16607.1025216615,16220.8541241609,15837.8515679465,15458.0887088975,15081.5477644531,14708.2061690694,14338.0500775912,13971.0562290607,13607.1963004363,13246.4473516155,12888.7801509908,12534.1635292784,12182.5684686355,11833.9636844194,11488.3172198557,11145.5969650376,10805.7708081153,10468.8068300842,10134.6735365985,9803.3392926197,9474.7732713214,9148.9454669403,8825.8250552529,8505.3835842148,8187.5928725691,7872.4243294653,7559.8509520542,7249.8458861630,6942.3833085896,6637.4378265350,6334.9850677737,6035.0006778394,5737.4603644125,5442.3406300771,5149.9827319463,4861.4402848198,4578.3062377667,4301.9399834781,4033.2433181982,3773.0060230056,3521.8995647162,3280.4752514175,3049.1642690842,2828.2810754964,2618.0294296530,2418.5075824695,2229.7175088433,2051.5753284917,1883.9193146682,1726.5212387473,1579.0978323314,1441.3181969590,1312.8153127827,1193.1951432829,1082.0429248404,978.9312212192,883.4271524030,795.0968573945,713.5103920015,638.2459582444,568.8927204503,505.0539122959,446.3471272144,392.4077580309,342.8892241721,297.4631385151,255.8194769287,217.6660756373,182.7303318654,150.7565010082,121.5063720715,94.7576755780,70.3045992144,47.9562863048,27.5372203559,8.8923220126"

# North Atlantic-European region
lonlatbox="-90,40,20,80"
lonlatboxstr="90w40e20n80n"

# store output in /work/bb1152/Module_A/A6_CyclEx/b380490_Albern/CMIP6_anomalies_mlev
cd /work/bb1152/Module_A/A6_CyclEx/b380490_Albern/CMIP6_anomalies_mlev

#######################################################################
# historical simulations
exp="historical"

# path to historical simulation
path_hist=$'/pool/data/CMIP6/data/CMIP/'$institute$'/'$model$'/'$exp$'/r1i1p1f1/6hrLev/ta/gn/v20190710'
echo $path_hist

# files needed for historical period
file_hist1=$'ta_6hrLev_'$model$'_'$exp$'_r1i1p1f1_gn_197001010600-199001010000.nc'
file_hist2=$'ta_6hrLev_'$model$'_'$exp$'_r1i1p1f1_gn_199001010600-201001010000.nc'
file_hist3=$'ta_6hrLev_'$model$'_'$exp$'_r1i1p1f1_gn_201001010600-201501010000.nc'
#echo $file_hist1
#echo $file_hist2
#echo $file_hist3

# calculate monthly-mean data
ofile1=${file_hist1%.*}$'.monmean.nc'
ofile2=${file_hist2%.*}$'.monmean.nc'
ofile3=${file_hist3%.*}$'.monmean.nc'
#echo $ofile1
#echo $ofile2
#echo $ofile3
#cdo monmean $path_hist/$file_hist1 $ofile1
#cdo monmean $path_hist/$file_hist2 $ofile2
#cdo monmean $path_hist/$file_hist3 $ofile3

# merge the two files
ofile_merge=${ofile1%-*}$'-'${ofile3##*-}
#echo $ofile_merge
cdo mergetime $ofile1 $ofile2 $ofile3 $ofile_merge

# extract 1980-2010 and calculate seasonal mean
ofile_seasons=$'ta_Lev_'$model$'_'$exp$'_r1i1p1f1_1980-81to2009-10.seasmean.nc'
echo $ofile_seasons
cdo seasmean -selyear,1980/2010 $ofile_merge $ofile_seasons

# get time mean over DJF season
# Note: output of seasmean contains timesteps for Jan, Apr, July, October
# (and December for last year). We only keep the January timesteps as 
# this is the DJF mean.
# We do not use the first timestep because this is only a Jan-Feb mean
# and not a "full" winter. Thus, we keep winter 1980/81 - 2009/10
# (=30 winter seasons).
ofile_djf=${ofile_seasons%.*}$'.DJF.timmean.nc'
echo $ofile_djf
cdo timmean -selmon,1 -seltimestep,2/125 $ofile_seasons $ofile_djf

# remap data to 1deg x 1deg resolution and cut out the North Atlantic-European region
ofile_naeu=${ofile_djf%.*}$'.remapcon.'$lonlatboxstr$'.nc'
echo $ofile_naeu
cdo -s sellonlatbox,$lonlatbox -remapcon,r360x180 $ofile_djf $ofile_naeu

# interpolate hypbrid sigma pressure coordinate to height levels needed for ICON simulation
ofile_hl=${ofile_naeu%.*}$'.ml2hl.nc'
echo $ofile_hl
cdo ml2hl,$z $ofile_naeu $ofile_hl

# calculate zonal mean
ofile_zm=${ofile_hl%.*}$'.zonmean.nc'
echo $ofile_zm
cdo zonmean $ofile_hl $ofile_zm

# change filenames to a shorter version
file_hist=${ofile_hl%%.*}$'.DJFmean.'$lonlatboxstr$'.nc'
file_hist_zm=${ofile_hl%%.*}$'.DJFmean.'$lonlatboxstr$'.zonmean.nc'
mv $ofile_hl $file_hist
mv $ofile_zm $file_hist_zm

# delete auxiliary data (keep monthly mean data because computing
# it takes about 30min per file)
###rm $ofile1 $ofile2 $ofile3
###rm $ofile_merge
rm $ofile_seasons $ofile_djf $ofile_naeu

echo
echo

#####################################################################
# path to SSP585 simulation
path_ssp=/pool/data/CMIP6/data/ScenarioMIP/MPI-M/MPI-ESM1-2-LR/ssp585/r1i1p1f1/6hrLev/ta/gn/v20190710

# loop over near future and far future time periods
futures=( "near" "far" )
for future in "${futures[@]}"; do
echo $future
if [ "$future" == "near" ]; then
    # files needed for near future period 
    file1=ta_6hrLev_MPI-ESM1-2-LR_ssp585_r1i1p1f1_gn_201501010600-203501010000.nc
    file2=ta_6hrLev_MPI-ESM1-2-LR_ssp585_r1i1p1f1_gn_201501010600-203501010000.nc
    file3=ta_6hrLev_MPI-ESM1-2-LR_ssp585_r1i1p1f1_gn_203501010600-205501010000.nc
elif [ "$future" == "far" ]; then
    # files needed for far future period
    file1=ta_6hrLev_MPI-ESM1-2-LR_ssp585_r1i1p1f1_gn_205501010600-207501010000.nc
    file2=ta_6hrLev_MPI-ESM1-2-LR_ssp585_r1i1p1f1_gn_207501010600-209501010000.nc
    file3=ta_6hrLev_MPI-ESM1-2-LR_ssp585_r1i1p1f1_gn_209501010600-210101010000.nc
fi

# calculate monthly-mean data
ofile1=${file1%.*}$'.monmean.nc'
ofile2=${file2%.*}$'.monmean.nc'
ofile3=${file3%.*}$'.monmean.nc'
#echo $ofile1
#echo $ofile2
#echo $ofile3
#cdo monmean $path_ssp/$file1 $ofile1
#cdo monmean $path_ssp/$file2 $ofile2
#cdo monmean $path_ssp/$file3 $ofile3

# merge the files for near future and the files for far future
ofile_merge=${ofile1%-*}$'-'${ofile3##*-}
echo $ofile_merge
cdo mergetime $ofile1 $ofile2 $ofile3 $ofile_merge

# extract 2020-2050 and 2070-2100, respectively, and calculate seasonal mean
if [ "$future" == "near" ];then
    ofile_seasons=$'ta_Lev_'$model$'_ssp585_r1i1p1f1_2020-21to2049-50.seasmean.nc'
    echo $ofile_seasons
    cdo seasmean -selyear,2020/2050 $ofile_merge $ofile_seasons
elif [ "$future" == "far" ]; then
    ofile_seasons=$'ta_Lev_'$model$'_ssp585_r1i1p1f1_2070-71to2099-00.seasmean.nc'
    echo $ofile_seasons
    cdo seasmean -selyear,2070/2100 $ofile_merge $ofile_seasons
fi

# get time mean over DJF season
# Note: As for the historical simulation, we do not use the first
# timestep of the seasmean file and keep 30 winter seasons.
ofile_djf=${ofile_seasons%.*}$'.DJF.nc'
echo $ofile_djf
cdo timmean -selmon,1 -seltimestep,2/125 $ofile_seasons $ofile_djf

# remap data to 1deg x 1deg resolution and cut out the North Atlantic-European region
ofile_naeu=${ofile_djf%.*}$'.remapcon.'$lonlatboxstr$'.nc'
echo $ofile_naeu
cdo -s sellonlatbox,$lonlatbox -remapcon,r360x180 $ofile_djf $ofile_naeu

# interpolate hypbrid sigma pressure coordinate to height levels needed for ICON simulation
ofile_hl=${ofile_naeu%.*}$'.ml2hl.nc'
echo $ofile_hl
cdo ml2hl,$z $ofile_naeu $ofile_hl

# calculate zonal mean
ofile_zm=${ofile_hl%.*}$'.zonmean.nc'
echo $ofile_zm
cdo zonmean $ofile_hl $ofile_zm

# change filenames to a shorter version
if [ "$future" == "near" ];then
    file_near=${ofile_hl%%.*}$'.DJFmean.'$lonlatboxstr$'.nc'
    file_near_zm=${ofile_hl%%.*}$'.DJFmean.'$lonlatboxstr$'.zonmean.nc'
    mv $ofile_hl $file_near
    mv $ofile_zm $file_near_zm
elif [ "$future" == "far" ]; then
    file_far=${ofile_hl%%.*}$'.DJFmean.'$lonlatboxstr$'.nc'
    file_far_zm=${ofile_hl%%.*}$'.DJFmean.'$lonlatboxstr$'.zonmean.nc'
    mv $ofile_hl $file_far
    mv $ofile_zm $file_far_zm
fi

# delete auxiliary data
###rm $ofile1 $ofile2 $ofile3
###rm $ofile_merge
rm $ofile_seasons $ofile_djf $ofile_naeu

echo
echo

done


#######################################################################
# subtract near future and historical and far future and historical
# to get the temperature anomalies

cdo sub $file_near_zm $file_hist_zm $model$'_nearfuture-historical_zm.nc'
cdo sub $file_far_zm $file_hist_zm $model$'_farfuture-historical_zm.nc'
#
