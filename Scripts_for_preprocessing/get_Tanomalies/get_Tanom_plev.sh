#!/bin/bash
# Prepare monthly-mean model output on pressure levels for ICON simulation
# historical:  1980-2010 -> winter 1980/81 - 2009/10
# near future: 2020-2050 -> winter 2020/21 - 2049/50
# far future:  2070-2100 -> winter 2070/71 - 2099/2100


# models, for which historical and ssp585 data is available in
# /work/bb1152/Module_A/A6_CyclEx/b380543_lentink/Environment_for_ICON/data_clim/clim3D
# (as of July 2021)
# model         ensemble member
# BCC-CSM2-MR   r1i1p1f1
# CNRM-CM6-1    r1i1p1f2
# CNRM-ESM2-1   r1i1p1f2
# MIROC6        r1i1p1f1
# MPI-ESM1-2-LR r1i1p1f1
# MRI-ESM2-0    r1i1p1f1

# decide, for wich model and ensemble member the temperature anomalies
# are calculated
model="MRI-ESM2-0" #"MPI-ESM1-2-LR"
ensmem="r1i1p1f1" #"r1i1p1f1"

# North Atlantic-European region
lonlatbox="-90,40,20,80"
lonlatboxstr="90w40e20n80n"

# store output in /work/bb1152/Module_A/A6_CyclEx/b380490_Albern/CMIP6_anomalies_plev
cd /work/bb1152/Module_A/A6_CyclEx/b380490_Albern/CMIP6_anomalies_plev

# path to data that was prepared by Hilke
path="/work/bb1152/Module_A/A6_CyclEx/b380543_lentink/Environment_for_ICON/data_clim/clim3D"
#echo $path

#######################################################################
# historical simulation
exp="historical"
echo $exp

# merge years 1980-2010
# input data is stored with this syntax:
# clim3D_Amon_<MODEL>_<EXP>_<ENSMEM>_gn_<YEAR>_90w40e20n80n.nc or
# clim3D_Amon_<MODEL>_<EXP>_<ENSMEM>_gr_<YEAR>_90w40e20n80n.nc
ofile_merge=$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_1980-81to2009-10.nc"
#echo $ofile_merge
cdo mergetime $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_198"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_199"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_200"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_2010_90w40e20n80n.nc" $ofile_merge

# calculate seasonal mean
ofile_seasons=${ofile_merge%.*}$".seasmean.nc"
#echo $ofile_seasons
cdo seasmean $ofile_merge $ofile_seasons

# get time mean over DJF season
# Note: output of seasmean contains timesteps for Jan, Apr, July and
# October for each year and and December for the last year. We only
# keep the January timesteps as this is the DJF mean.
# We do not use the first timestep because this is only a Jan-Feb mean
# and not a "full" winter. Thus, we keep winter 1980/81 - 2009/10
# (=30 winter seasons).
ofile_djf=${ofile_seasons%.*}$".DJF.timmean.nc"
#echo $ofile_djf
cdo timmean -selmon,1 -seltimestep,2/125 $ofile_seasons $ofile_djf

# remap data to grid from initial file and cut out the
# North Atlantic-European region
ofile_1deg=${ofile_djf%.*}$".remapcon."$lonlatboxstr$".nc"
#echo $ofile_1deg
cdo -s sellonlatbox,$lonlatbox -remapcon,r360x360 $ofile_djf $ofile_1deg

# calculate the zonal mean
ofile_zm=${ofile_1deg%.*}$".zonmean.nc"
#echo $ofile_zm
cdo zonmean $ofile_1deg $ofile_zm

# change filenames to a shorter version
file_hist=${ofile_1deg%%.*}$".DJFmean."$lonlatboxstr$".nc"
file_hist_zm=${ofile_1deg%%.*}$".DJFmean."$lonlatboxstr$".zonmean.nc"
mv $ofile_1deg $file_hist
mv $ofile_zm $file_hist_zm

# delete auxiliary data
rm $ofile_merge $ofile_seasons $ofile_djf

echo

#####################################################################
# SSP585 simulation
exp="ssp585"

# loop over near future and far future time periods
futures=( "near" "far" )
for future in "${futures[@]}"; do
echo $future
if [ "$future" == "near" ]; then
    # merge years 2020-2050
    ofile_merge=$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_2020-21to2049-50.nc"
    #echo $ofile_merge
    cdo mergetime $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_202"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_203"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_204"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_2050_90w40e20n80n.nc" $ofile_merge
elif [ "$future" == "far" ]; then
    # merge years 2070-2100
    ofile_merge=$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_2070-71to2099-00.nc"
    #echo $ofile_merge
    cdo mergetime $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_207"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_208"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_209"*$"_90w40e20n80n.nc" $path/$"clim3D_Amon_"$model$"_"$exp$"_"$ensmem$"_g"*$"_2100_90w40e20n80n.nc" $ofile_merge
fi

# calculate seasonal mean
ofile_seasons=${ofile_merge%.*}$".seasmean.nc"
#echo $ofile_seasons
cdo seasmean $ofile_merge $ofile_seasons

# get time mean over DJF season
# Note: As for the historical simulation, we do not use the first
#       timestep of the seasmean file and keep 30 winter seasons.
ofile_djf=${ofile_seasons%.*}$".DJF.timmean.nc"
#echo $ofile_djf
cdo timmean -selmon,1 -seltimestep,2/125 $ofile_seasons $ofile_djf

# remap data to grid from initial file and cut out the
# North Atlantic-European region
ofile_1deg=${ofile_djf%.*}$".remapcon."$lonlatboxstr$".nc"
#echo $ofile_1deg
cdo -s sellonlatbox,$lonlatbox -remapcon,r360x360 $ofile_djf $ofile_1deg

# calculate the zonal mean
ofile_zm=${ofile_1deg%.*}$".zonmean.nc"
#echo $ofile_zm
cdo zonmean $ofile_1deg $ofile_zm

# change filenames to a shorter version
if [ "$future" == "near" ];then
    file_near=${ofile_1deg%%.*}$".DJFmean."$lonlatboxstr$".nc"
    file_near_zm=${ofile_1deg%%.*}$".DJFmean."$lonlatboxstr$".zonmean.nc"
    mv $ofile_1deg $file_near
    mv $ofile_zm $file_near_zm
elif [ "$future" == "far" ]; then
    file_far=${ofile_1deg%%.*}$".DJFmean."$lonlatboxstr$".nc"
    file_far_zm=${ofile_1deg%%.*}$".DJFmean."$lonlatboxstr$".zonmean.nc"
    mv $ofile_1deg $file_far
    mv $ofile_zm $file_far_zm
fi

# delete auxiliary data
rm $ofile_merge $ofile_seasons $ofile_djf

echo

done


#######################################################################
# subtract near future and historical and far future and historical
# to get the temperature anomalies
echo "Subtract future and historical"

cdo sub $file_near_zm $file_hist_zm $model$"_nearfuture-historical_zm.nc"
cdo sub $file_far_zm $file_hist_zm $model$"_farfuture-historical_zm.nc"

echo "DONE!"
#
