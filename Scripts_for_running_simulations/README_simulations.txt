ICON-NWP aquaplanet simulations of a baroclinic lifecycle (9 days)
in limited area mode with channel setup.

# 2km horizontal resolution
channel_2km_0001: control simulation
channel_2km_0002: +4K atmospheric and surface temperatures, qv consistent with T
channel_2km_0003: +4K temperatures, qv from control simulation
channel_2km_0004: +temperature anomaly from MPI-ESM1-2-LR far future
channel_2km_0005: +tropical temperature anomaly from MPI-ESM1-2-LR far future
channel_2km_0006: +polar temperature anomaly from MPI-ESM1-2-LR far future

# 80km horizontal resolution
channel_80km_0001: control simulation
channel_80km_0002: +4K atmospheric and surface temperatures, qv consistent with T
channel_80km_0003: +4K temperatures, qv from control simulation
channel_80km_0004: +temperature anomaly from MPI-ESM1-2-LR far future
channel_80km_0005: +tropical temperature anomaly from MPI-ESM1-2-LR far future
channel_80km_0006: +polar temperature anomaly from MPI-ESM1-2-LR far future
